"""Auto airflow.cfg editor
Sets everything to the state it is supposed to be in.
"""

import os
from configparser import ConfigParser

from cryptography.fernet import Fernet

HOME = os.environ["HOME"]
AIRFLOW_PATH = os.getenv("AIRFLOW_HOME", os.path.join(HOME, "airflow"))
HOSTNAME = os.getenv("BIZZFLOW_HOSTNAME", "localhost")
POSTGRES_USER = os.getenv("POSTGRES_USER")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
POSTGRES_HOST = os.getenv("POSTGRES_HOST")
POSTGRES_DB = os.getenv("POSTGRES_DB")

airflow_path = os.path.join(AIRFLOW_PATH, "airflow.cfg")

DAGS_FOLDER = os.getenv("AIRFLOW__CORE__DAGS_FOLDER") or os.path.join(HOME, "dags")
PLUGINS_FOLDER = os.getenv("AIRFLOW__CORE__PLUGINS_FOLDER") or os.path.join(HOME, "airflow-plugins")
ALCHEMY_CONN = f"postgresql+psycopg2://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}/{POSTGRES_DB}"
WEB_SERVER_PORT = 8080

# Internal port number breaks links when behind reverse proxy
BASE_URL = f"http://localhost:{WEB_SERVER_PORT}" if HOSTNAME == "localhost" else f"http://{HOSTNAME}"
ENDPOINT_URL = f"http://localhost:{WEB_SERVER_PORT}"
FERNET_KEY = Fernet.generate_key().decode()
SECRET_KEY = Fernet.generate_key().decode()


airflow_conf = {
    "core": {
        "executor": "LocalExecutor",
        "dags_folder": DAGS_FOLDER,
        "load_examples": str(False),
        "load_default_connections": str(False),
        "fernet_key": FERNET_KEY,
        "plugins_folder": PLUGINS_FOLDER,
    },
    "database": {
        "sql_alchemy_conn": ALCHEMY_CONN,
    },
    "cli": {
        "endpoint_url": ENDPOINT_URL,
    },
    "webserver": {
        "base_url": BASE_URL,
        "web_server_port": str(WEB_SERVER_PORT),
        "enable_proxy_fix": str(True),
        "secret_key": SECRET_KEY,
        "instance_name": "Bizzflow",
    },
    "smtp": {
        "smtp_host": os.getenv("SMTP_HOST"),
        "smtp_starttls": os.getenv("SMTP_STARTTLS"),
        "smtp_ssl": os.getenv("SMTP_SSL"),
        "smtp_user": os.getenv("SMTP_USER"),
        "smtp_password": os.getenv("SMTP_PASSWORD"),
        "smtp_port": os.getenv("SMTP_PORT"),
        "smtp_mail_from": os.getenv("SMTP_MAIL_FROM"),
    },
    "scheduler": {
        "catchup_by_default": str(False),
        "min_file_process_interval": str(60),
        "parsing_processes": str(1),
    },
    "metrics": {
        "statsd_on": str(True),
        "statsd_host": "host.docker.internal",
        "statsd_allow_list": "scheduler,dag_processing,executor",
    },
    "api": {
        "auth_backend": "airflow.api.auth.backend.basic_auth",
    },
}

logging_config = {
    "logging": {
        "remote_logging": str(False),
    }
}

PLATFORM = os.getenv("PLATFORM")
BUCKET_LOGS = os.getenv("BUCKET_LOGS")
REMOTE_BASE_LOG_FOLDER = ""
REMOTE_LOG_CONN_ID = ""
if PLATFORM == "gcp":
    REMOTE_BASE_LOG_FOLDER = f"gs://{BUCKET_LOGS}/"
    REMOTE_LOG_CONN_ID = "google_application_credentials"
elif PLATFORM == "aws":
    REMOTE_BASE_LOG_FOLDER = f"s3://{BUCKET_LOGS}/"
    REMOTE_LOG_CONN_ID = "AWS_credentials"
elif PLATFORM == "azure":
    # REMOTE_BASE_LOG_FOLDER needs to start with "wasb"
    REMOTE_BASE_LOG_FOLDER = "wasb"
    REMOTE_LOG_CONN_ID = "blob_storage_account_key"
    logging_config["azure_remote_logging"] = {
        "remote_wasb_log_container": BUCKET_LOGS,
    }

logging_config = {
    "logging": {
        "remote_logging": str(True),
        "remote_base_log_folder": REMOTE_BASE_LOG_FOLDER,
        "remote_log_conn_id": REMOTE_LOG_CONN_ID,
    }
}

airflow_conf.update(logging_config)

conf = ConfigParser()
conf.read_dict(airflow_conf)

with open(airflow_path, "w", encoding="utf-8") as outfile:
    conf.write(outfile)
