#!/bin/bash

bash ./bizzflow_init.sh
python airflow_config.py
airflow db init

connection_exists() {
    return $(python3 -c 'import sys
from airflow.settings import Session
from airflow.models import Connection
s = Session()
sys.exit(
    1 if s.query(Connection).filter(
        Connection.conn_id=="'$1'"
        ).first() is None else 0
    )' >/dev/null 2>&1)
}

echo Creating default Admin user ${WEB_USER_USERNAME}
airflow users create -r Admin -u ${WEB_USER_USERNAME} -f Bizzflow -l Administrator -p ${WEB_USER_PASSWORD} -e ${WEB_USER_EMAIL}

if ! connection_exists "classicmodels"; then
    echo Creating default connections
    airflow connections add 'classicmodels' --conn-password 'relational' --conn-type MySQL
fi

# Create GCP connection if needed
if [ "$PLATFORM" == "gcp" ]; then
    if ! connection_exists "google_application_credentials"; then
        SERVICE_ACCOUNT_EMAIL=$(gcloud config get-value account)
        gcloud iam service-accounts keys create \
            /tmp/google_application_credentials.json \
            --iam-account=${SERVICE_ACCOUNT_EMAIL}
        airflow connections add "google_application_credentials" \
            --conn-password "$(cat /tmp/google_application_credentials.json)" \
            --conn-type google_cloud_platform
    fi
fi

# Snowflake connections
if [ "$PLATFORM" == "aws" ]; then
    if ! connection_exists "snowflake-ORCHESTRATOR"; then
        airflow connections add "snowflake-ORCHESTRATOR" \
            --conn-password ''"${SNOWFLAKE_ORCHESTRATOR}"'' \
            --conn-type snowflake

        airflow connections add "snowflake-ROLE_CREATOR" \
            --conn-password ''"${SNOWFLAKE_ROLE_CREATOR}"'' \
            --conn-type snowflake

        airflow connections add "snowflake-ROLE_MANAGER" \
            --conn-password ''"${SNOWFLAKE_ROLE_MANAGER}"'' \
            --conn-type snowflake

        airflow connections add "snowflake-USER_MANAGER" \
            --conn-password ''"${SNOWFLAKE_USER_MANAGER}"'' \
            --conn-type snowflake
    fi
    aws configure --profile default set output json
    aws configure --profile default set region "${COMPUTE_REGION}"
    if ! connection_exists "AWS_credentials"; then
        airflow connections add "AWS_credentials" \
            --conn-login ''"${AWS_S3_ACCESS_KEY}"'' \
            --conn-password ''"${AWS_S3_SECRET_KEY}"'' \
            --conn-type amazon_web_services
    fi
    if ! connection_exists "aws_access_key_id"; then
        airflow connections add "aws_access_key_id" \
            --conn-password ''"${AWS_S3_ACCESS_KEY}"'' \
            --conn-type amazon_web_services
    fi
    if ! connection_exists "aws_secret_access_key"; then
        airflow connections add "aws_secret_access_key" \
            --conn-password ''"${AWS_S3_SECRET_KEY}"'' \
            --conn-type amazon_web_services
    fi
fi

# AzureSQL connections
if [ "$PLATFORM" == "azure" ]; then
    if ! connection_exists "azure-sql-ORCHESTRATOR"; then
        airflow connections add "azure-sql-ORCHESTRATOR" \
            --conn-password ''"${AZURESQL_ORCHESTRATOR}"'' \
            --conn-type microsoft_sql_server

        airflow connections add "azure-sql-ROLE_CREATOR" \
            --conn-password ''"${AZURESQL_ROLE_CREATOR}"'' \
            --conn-type microsoft_sql_server

        airflow connections add "azure-sql-ROLE_MANAGER" \
            --conn-password ''"${AZURESQL_ROLE_MANAGER}"'' \
            --conn-type microsoft_sql_server

        airflow connections add "azure-sql-USER_MANAGER" \
            --conn-password ''"${AZURESQL_USER_MANAGER}"'' \
            --conn-type microsoft_sql_server

        airflow connections add "blob_storage_account_key" \
            --conn-login ''"${AZURE_STORAGE_ACCOUNT_NAME}"'' \
            --conn-password ''"${BLOB_STORAGE_ACCOUNT_KEY}"'' \
            --conn-type wasb
    fi
    az login --identity
fi

# Onprem Connections
if [ "$PLATFORM" == "onprem" ]; then
    if ! connection_exists "postgresql-ORCHESTRATOR"; then
        airflow connections add "postgresql-ORCHESTRATOR" \
            --conn-password ''"${POSTGRES_ORCHESTRATOR_PASSWORD}"'' \
            --conn-type postgresql

        airflow connections add "postgresql-USER_MANAGER" \
            --conn-password ''"${POSTGRES_USER_MANAGER_PASSWORD}"'' \
            --conn-type postgresql
    fi
fi
