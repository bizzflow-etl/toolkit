# How to handle migration to Airflow 2.0

## Step 1: Upgrade to bizzflow to version 2.3.0 first

## Step 2: Stop bizzflow

```bash
docker-compose -p bizzflow down
```

## Step 3: Backup metadata database

## Step 4: Update airflow config

rbac must be true, logging and metrics as separate category

```
[webserver]
rbac = True
.....
separate logging and metrics
[logging]
remote_logging = False
remote_base_log_folder =
remote_log_conn_id =
....
[metrics]
statsd_on = True
statsd_host = host.docker.internal
```

## Step 5: Do changes in connection and check upgrade

Use bizzflow-init container for it (all volumes and env are present), just run it as root

```bash
docker-compose -p bizzflow run --user 0000 bizzflow-init bash
```

in docker container add airflow folder to env

```bash
export AIRFLOW_HOME="/home/bizzflow/airflow"
```

Change connection typo to bizzflow_vault (in new version of bizzflow is supported), main reaso nfor it is that Airflow
do not support connection without type
it will done by following python script or run sql query directly in database

```python3

import os

from sqlalchemy_utils.types.pg_composite import psycopg2

POSTGRES_USER = os.getenv("POSTGRES_USER")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
POSTGRES_HOST = os.getenv("POSTGRES_HOST")
POSTGRES_DB = os.getenv("POSTGRES_DB")
conn = psycopg2.connect(dbname=POSTGRES_DB, user=POSTGRES_USER, password=POSTGRES_PASSWORD, host=POSTGRES_HOST)
cur = conn.cursor()
cur.execute("UPDATE connection SET conn_type='bizzflow_vault' WHERE conn_type='' OR conn_type IS NULL;")
conn.commit()
cur.close()
```

install airflow-upgrade-check

```bash
pip install apache-airflow-upgrade-check
```

run upgrade check

```bash
airflow upgrade_check
```

it will surelly fails, but if it fails just for controling DAGs validity - in old version of bizzflow we use unsuported kwargs argument.

## Step 6: Upgrade to Airflow 2.0

change .evn to use toolkit version 2.4.x
and update it

```bash
docker-compose -p bizzflow pull
```

Then run migration from bizzflow-init container

```bash
docker-compose -p bizzflow run bizzflow-init bash
```

```bash
airflow db upgrade
```

## Step 7: Update airflow config

database is separate category, remove rbac from config, you can add instance_name = .... to webserver section

```

[database]
sql_alchemy_conn =

..... 
----rbac = True---
```

## Step 8: Start bizzflow

```bash
GIT_SSH_KEY=`cat id_git` docker-compose -p bizzflow up --remove-orphans -d airflow-webserver flow-ui
```

## Step 9: Check if everything is working and remove bad data form db

airflow can show following warnings or similar

```
While upgrading the metadatabase, Airflow had to move some bad data in order to apply new constraints. The moved data can be found in the following tables:
Source table	Table with moved rows
task_fail	_airflow_moved__2_3__duplicates__task_fail
Please inspect the moved data to decide whether you need to keep them, and manually drop the moved tables to dismiss this warning. Read more about it in Upgrading.
```

You can delete it from docker container
```bash
docker-compose -p bizzflow run bizzflow-init python
```

```python3

import os

from sqlalchemy_utils.types.pg_composite import psycopg2

POSTGRES_USER = os.getenv("POSTGRES_USER")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
POSTGRES_HOST = os.getenv("POSTGRES_HOST")
POSTGRES_DB = os.getenv("POSTGRES_DB")
conn = psycopg2.connect(dbname=POSTGRES_DB, user=POSTGRES_USER, password=POSTGRES_PASSWORD, host=POSTGRES_HOST)
cur = conn.cursor()
cur.execute("DROP TABLE _airflow_moved__2_3__dangling__task_fail")
conn.commit()
cur.close()
```
```