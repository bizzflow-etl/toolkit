# Bizzflow R2 Toolkit

![alt text][bizztreat-logo]

Bizzflow R2 Toolkit is a version 2 of Bizzflow ETL Pipeline template written mostly in Python, using [Apache Airflow](https://airflow.apache.org/) as a scheduler and job executor. Bizzflow is meant to work in cloud, meaning any of the major cloud providers ([Google Cloud Platform](https://cloud.google.com), [Amazon Web Services](https://aws.amazon.com) and [Microsoft Azure](https://azure.microsoft.com)). If you would like to implement Bizzflow solution in your cloud, do not hesitate to contact [Bizztreat admin](https://bizztreat.com).


## Contributing

- [Building base image](doc/building_base.md)
- [Dependency management](doc/dependency_management.md)


## Crossroads

- [Bizzflow R2 Toolkit Documentation](doc/README.md)
- [Installation](doc/README.md#installation)
- [Python API Reference](https://bizztreat.gitlab.io/bizzflow/r2/toolkit/index.html)
- [Bizztreat](https://bizztreat.com)

## Installation

See [Installation instructions](doc/README.md#installation) for more information.

## License

**TODO:** To be decided yet.

See details in [LICENSE](./LICENSE).

[bizztreat-logo]: ./doc/bizztreatlogo.png "Bizztreat Logo"
