# Dependency management
We use [Poetry](https://python-poetry.org/) to manage the dependencies.
[Read why](https://github.com/python-poetry/poetry#why) we prefer Poetry instead of Pipenv.

## Installing poetry
If you are going to install poetry don't install it into you local virtualenv (it might mess up some of your dependencies).
It is better to use the official procedure or `pipx`. See https://python-poetry.org/docs/#installation

## Installing dependencies
To make your dependencies up to date run
```bash
poetry install
```

## Adding new dependency
- Install the dependency  `poetry add package_name`
- Run `./update_requirements.sh` to update `requirements.txt`. During deployment, pip is used to install requirements. So without this step your changes will not be propagated to servers.

# Updating all dependencies
- Run `poetry update` without any changes to `pyproject.toml` to simply update all deps.
- Run `./update_requirements.sh`

