#!/bin/bash

rm -rf docvenv
python3 -m venv docvenv
. venv/bin/activate
python3 -m pip install --upgrade pip setuptools
cd ..
python3 -m pip install -e .
cd doc
pdoc3 --html -o tmpdoc toolkit
mv tmpdoc/toolkit/*
./reference/
deactivate
rm -rf docvenv
