# Building base image

Anytime system-wide or Python dependencies change, you should rebuild a base image in order to make
building of target images a little bit faster.

Base images reside in `registry.gitlab.com/bizztreat/bizzflow/r2/toolkit/base`.

## Versioning

Base images use different versioning from `toolkit`, see
[image tags](https://gitlab.com/bizztreat/bizzflow/r2/toolkit/container_registry/3042556) for
latest version number,

## Building

Build image using `base.Dockerfile`.

```console
docker build -t registry.gitlab.com/bizztreat/bizzflow/r2/toolkit/base:<VERSION> -f base.Dockerfile .
```

Then push it to the registry.

```console
registry.gitlab.com/bizztreat/bizzflow/r2/toolkit/base:<VERSION>
```

If applicable, do not forget to change the first line in `Dockerfile`, such as:

```dockerfile
FROM registry.gitlab.com/bizzflow-etl/toolkit/base:<VERSION>
...
```
