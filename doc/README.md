# Bizzflow R2 Toolkit Documentation

| Version | Authors | Last modified |
| ------- | ------- | ------------- |
| 2.0.0 | Tomáš Votava, Petra Horáčková, Elena Jánošíková | 17. 4. 2020 |

Official Bizzflow Release 2 Toolkit Documentation.

## API Reference

[Python API reference](https://bizztreat.gitlab.io/bizzflow/r2/toolkit/) is mainained individually. Bizzflow Toolkit is self-documented extensively using pydoc [PEP 257](https://www.python.org/dev/peps/pep-0257/), you can get a lot of information just by browsing the [source](../toolkit).

## Installation
