#!/bin/bash

# distill ssh key from env variable
if [ ! -f "~/.ssh/id_rsa" ]; then
  mkdir -p ~/.ssh/
  echo -e "${GIT_SSH_KEY}" | tee ~/.ssh/id_rsa >/dev/null
  chmod 400 ~/.ssh/id_rsa
fi

# Init bizzflow project
if [ -d "$BIZZFLOW_PROJECT_PATH" ] && cd $BIZZFLOW_PROJECT_PATH && git log -1; then
  echo "Bizzflow project already exists in ${BIZZFLOW_PROJECT_PATH} - skipping git cloning. Showing last commit"
else
  if [ -d "$BIZZFLOW_PROJECT_PATH" ]; then
    cd && rm -R $BIZZFLOW_PROJECT_PATH
  fi
  echo "Cloning project config files from ${BIZZFLOW_PROJECT_GIT} to ${BIZZFLOW_PROJECT_PATH}"
  git clone "$BIZZFLOW_PROJECT_GIT" "$BIZZFLOW_PROJECT_PATH"
  echo "Git cloned to to  ${BIZZFLOW_PROJECT_PATH}..."
  ln -s $BIZZFLOW_PROJECT_PATH/dags/ /home/bizzflow/dags/custom_dags
fi

cd $BIZZFLOW_PROJECT_PATH
if [[ ! -f "project.json" ]]; then
  echo "Empty project creating project from template"
  mkdir -p /tmp/base
  curl -L https://gitlab.com/bizzflow-etl/base-project/-/archive/master/base-project-master.tar | tar -x -C /tmp/base
  mv /tmp/base/base-project-master/* $BIZZFLOW_PROJECT_PATH
  cd /home/bizzflow && python bizzflow_config.py
  cd $BIZZFLOW_PROJECT_PATH
  git config --global user.email "owner@bizzflow.com"
  git config --global user.name "Bizzflow"
  git add .
  git commit -m "Bizzflow project init"
  git push
fi
