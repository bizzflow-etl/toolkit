#!/bin/bash

[ "${CONFIG_FILE}" == "" ] || [ "${CONTEXT_DIR}" == "" ] && {
    echo "CONFIG_FILE and CONTEXT_DIR vars must be set"
    echo "You shouldn't be running this script directly."
    echo 'Use one of ./start* files in the root directory instead'
    exit 1
}

[ -f "${CONFIG_FILE}" ] && {
    echo "Previous onprem configuration found in ${CONFIG_FILE}"
} || {
    echo "Generating onprem configuration in ${CONFIG_FILE}"
    cat <<EOT >>"${CONFIG_FILE}"
SMTP_HOST=email-smtp.us-east-1.amazonaws.com
SMTP_STARTTLS=True
HOSTNAME="localhost:8081"
SMTP_SSL=False
SMTP_USER=""
SMTP_PASSWORD=""
SMTP_PORT=587
SMTP_MAIL_FROM="onprem@bizzflow.app"
WEB_USER_USERNAME=bizzflow
WEB_USER_EMAIL="user@bizzflow.app"
WEB_USER_PASSWORD=wolfzzib
POSTGRES_ORCHESTRATOR_PASSWORD="$(
        openssl rand -base64 24
    )"
POSTGRES_USER_MANAGER_PASSWORD="$(
        openssl rand -base64 24
    )"
POSTGRES_HOST=172.17.0.1
ALLOW_DEV_USER=1
UID=0
WORKER_CONTEXT="${CONTEXT_DIR}"
EOT
    echo -e "\nCheck the generated ${CONFIG_FILE} file and edit it to your likings."
    echo "Press ENTER to continue..."
    read
}

[ -d "${CONTEXT_DIR}" ] && {
    echo "Previous context found in ${CONTEXT_DIR} - skipping initialization"
} || {
    echo "Creating context in ${CONTEXT_DIR}"
    mkdir -p "${CONTEXT_DIR}"
    mkdir -p "${CONTEXT_DIR}/postgres"
    mkdir -p "${CONTEXT_DIR}/worker-context"
    mkdir -p "${CONTEXT_DIR}/project"
    mkdir -p "${CONTEXT_DIR}/file-storage"
    [ "$UID" != "0" ] && {
        echo "WARNING: We have to make the context dir accessible for all users on this PC."
        chmod -R 777 "${CONTEXT_DIR}"
    }
    echo "Initializing Bizzflow"
    docker compose -f docker-compose.yaml -f docker-compose.init.yaml -p bizzflow --env-file "${CONFIG_FILE}" run bizzflow-init
    echo "Initializing PostgreSQL database"
    docker compose -f docker-compose.yaml -f docker-compose.init.yaml -p bizzflow --env-file "${CONFIG_FILE}" run postgres-init
}

docker compose -p bizzflow --env-file "${CONFIG_FILE}" watch

echo "Bizzflow should be running on http://localhost:8081"

echo "Stop it using:"
echo -e "docker compose -p bizzflow down"
