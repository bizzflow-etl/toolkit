#!/bin/bash

set -e

[ "$USERID" = "" ] && {
	echo "No USERID was set, I don't know what to do"
	exit 1
}

[ "$UID" != "0" ] && {
	echo "You should run this within target docker container as a root user"
}

[ `awk -F ':' '{ if ($3 == "'${USERID}'") print $3 }' /etc/passwd` ] && {
	echo "User with UID $USERID already exists in /etc/passwd"
} || {
	echo "Creating user information for UID $USERID in /etc/passwd"
	echo "bizzflow:x:${USERID}:${USERID}::/home/bizzflow:/bin/bash" >> /etc/passwd
}

