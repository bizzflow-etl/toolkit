CREATE DATABASE "bizzflow";
CREATE ROLE "USER_MANAGER" NOCREATEDB CREATEROLE NOINHERIT LOGIN PASSWORD '${POSTGRES_USER_MANAGER_PASSWORD}';
CREATE ROLE "ORCHESTRATOR" NOCREATEDB CREATEROLE NOINHERIT LOGIN PASSWORD '${POSTGRES_ORCHESTRATOR_PASSWORD}';
GRANT ALL PRIVILEGES ON DATABASE "bizzflow" TO "USER_MANAGER" WITH
GRANT OPTION;
GRANT ALL PRIVILEGES ON DATABASE "bizzflow" TO "ORCHESTRATOR";
