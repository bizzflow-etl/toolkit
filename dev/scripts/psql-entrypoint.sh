#!/bin/bash

[ $(which envsubst) ] || {
    apk add gettext
}

cat /psql-init.sql | envsubst >/psql-init-pass.sql

echo "postgres:5432:postgres:${POSTGRES_USER}:${POSTGRES_PASSWORD}" >>/root/.pgpass

PGPASSWORD=${POSTGRES_PASSWORD} psql \
    --host postgres \
    --user ${POSTGRES_USER} \
    --dbname postgres \
    --file=/psql-init-pass.sql
