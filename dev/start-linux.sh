#!/bin/bash

# This starts an on-prem instance of Bizzflow.
# We do not use a particularly safe way of generating password
# but considering all other things, I do not think this matters.

set -e -o pipefail

echo "WARNING: Bizzflow on-prem is not suitable for production use"

CONFIG_FILE=$(realpath ./onprem.env)
CONTEXT_DIR="$(realpath ./onprem/)/worker-context"

CONFIG_FILE="$CONFIG_FILE" CONTEXT_DIR="$CONTEXT_DIR" ./scripts/start-onprem.sh
