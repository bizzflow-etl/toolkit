#!/bin/zsh

# This starts an on-prem instance of Bizzflow.
# We do not use a particularly safe way of generating password
# but considering all other things, I do not think this matters.

set -e -o pipefail

DARWIN_SCRIPT_DIR="${0:a:h}"

echo "WARNING: Bizzflow on-prem is not suitable for production use"

CONFIG_FILE="${DARWIN_SCRIPT_DIR}/onprem.env"
CONTEXT_DIR="${DARWIN_SCRIPT_DIR}/onprem/worker-context"

CONFIG_FILE="$CONFIG_FILE" CONTEXT_DIR="$CONTEXT_DIR" ./scripts/start-onprem.sh