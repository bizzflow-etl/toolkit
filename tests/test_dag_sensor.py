import logging
from datetime import datetime, timedelta
from unittest import mock
from unittest.mock import MagicMock

from airflow import DAG
from airflow.models import DagBag
from airflow.utils import timezone
from airflow.utils.state import State

import pytest

from toolkit.airflow_plugin.plugin import ExternalDAGSensor

logger = logging.getLogger(__name__)


@pytest.fixture
def operator():
    dag = DAG(
        dag_id="my_dag_id",
        default_args={
            "owner": "bizzflow",
            "depends_on_past": False,
            "start_date": datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=31),
            "email_on_failure": False,
            "retries": 0,
            "retry_delay": timedelta(seconds=30),
            "catchup": False,
            "schedule_interval": None,
        },
    )

    kwargs = {}
    kwargs["external_dag_id"] = "my_subdag_id"
    kwargs["data_age"] = timedelta(minutes=10)
    kwargs["poke_interval"] = 20
    kwargs["mode"] = "reschedule"
    kwargs["timeout"] = 30

    operator = ExternalDAGSensor(task_id="my_task", dag=dag, **kwargs)
    return operator


def test_poke_unexisting_dag(operator):
    ti = MagicMock(start_date=datetime.now(), state=State.RUNNING)
    with mock.patch.object(DagBag, "get_dag") as mock_get_dag:
        mock_get_dag.return_value = None
        with pytest.raises(ValueError):
            operator.poke(context={"ti": ti})


def test_poke_never_run_dag(operator):
    now = timezone.utcnow()
    ti = MagicMock(start_date=now, state=State.RUNNING)
    with mock.patch.object(DagBag, "get_dag") as mock_get_dag:
        mock_dag = MagicMock()
        mock_dag.get_last_dagrun.return_value = None
        mock_get_dag.return_value = mock_dag
        poke_result = operator.poke(context={"ti": ti})
        assert poke_result is False
        mock_dag.create_dagrun.assert_called_once()


def test_poke_running_dag(operator):
    now = timezone.utcnow()
    ti = MagicMock(start_date=now, state=State.RUNNING)
    with mock.patch.object(DagBag, "get_dag") as mock_get_dag:
        mock_dag = MagicMock()
        last_dagrun = MagicMock(state=State.RUNNING, end_date=None, start_date=now - timedelta(minutes=5))
        mock_dag.get_last_dagrun.return_value = last_dagrun
        mock_get_dag.return_value = mock_dag
        poke_result = operator.poke(context={"ti": ti})
        assert poke_result is False
        mock_dag.create_dagrun.assert_not_called()


def test_poke_running_dag_out_of_range(operator):
    now = timezone.utcnow()
    ti = MagicMock(start_date=now, state=State.RUNNING)
    with mock.patch.object(DagBag, "get_dag") as mock_get_dag:
        mock_dag = MagicMock()
        last_dagrun = MagicMock(state=State.RUNNING, end_date=None, start_date=now - timedelta(minutes=50))
        mock_dag.get_last_dagrun.return_value = last_dagrun
        mock_get_dag.return_value = mock_dag
        poke_result = operator.poke(context={"ti": ti})
        assert poke_result is False
        mock_dag.create_dagrun.assert_not_called()


def test_poke_success_dag(operator):
    now = timezone.utcnow()
    ti = MagicMock(start_date=now, state=State.RUNNING)
    with mock.patch.object(DagBag, "get_dag") as mock_get_dag:
        mock_dag = MagicMock()
        last_dagrun = MagicMock(
            state=State.SUCCESS, end_date=now - timedelta(minutes=3), start_date=now - timedelta(minutes=5)
        )
        mock_dag.get_last_dagrun.return_value = last_dagrun
        mock_get_dag.return_value = mock_dag
        poke_result = operator.poke(context={"ti": ti})
        assert poke_result is True
        mock_dag.create_dagrun.assert_not_called()


def test_poke_success_dag_out_of_range(operator):
    now = timezone.utcnow()
    ti = MagicMock(start_date=now, state=State.RUNNING)
    with mock.patch.object(DagBag, "get_dag") as mock_get_dag:
        mock_dag = MagicMock()
        last_dagrun = MagicMock(
            state=State.SUCCESS, end_date=now - timedelta(minutes=30), start_date=now - timedelta(minutes=40)
        )
        mock_dag.get_last_dagrun.return_value = last_dagrun
        mock_get_dag.return_value = mock_dag
        poke_result = operator.poke(context={"ti": ti})
        assert poke_result is False
        mock_dag.create_dagrun.assert_called_once()


def test_poke_failed_dag(operator):
    now = timezone.utcnow()
    ti = MagicMock(start_date=now, state=State.RUNNING)
    with mock.patch.object(DagBag, "get_dag") as mock_get_dag:
        mock_dag = MagicMock()
        last_dagrun = MagicMock(
            state=State.FAILED, end_date=now - timedelta(minutes=3), start_date=now - timedelta(minutes=5)
        )
        mock_dag.get_last_dagrun.return_value = last_dagrun
        mock_get_dag.return_value = mock_dag
        poke_result = operator.poke(context={"ti": ti})
        assert poke_result is False
        mock_dag.create_dagrun.assert_called_once()
