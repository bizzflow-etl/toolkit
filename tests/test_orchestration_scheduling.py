from airflow.datasets import Dataset

import pytest

from tests.utils import mock_config_open
from toolkit.dags.helpers.utils import get_orchestration_schedule
from toolkit.managers.configuration.exceptions import ConfigurationNotValid
from toolkit.managers.configuration.v1.loader import V1ConfigurationLoader

valid_configs = [
    ("""[{"id":"o0","tasks":[{"id":"t0","type":"dummy"}], "schedule": null}]""", "", None),
    (
        """[{"id":"o0","tasks":[{"id":"t0","type":"dummy"}], "schedule": "0 0 * * *"}]""",
        """["dataset1", "dataset2"]""",
        "0 0 * * *",
    ),
    (
        """[{"id":"o0","tasks":[{"id":"t0","type":"dummy"}], "schedule": {"datasets": ["dataset1", "dataset2"]}}]""",
        """["dataset1", "dataset2"]""",
        [Dataset("bizzflow://dataset1"), Dataset("bizzflow://dataset2")],
    ),
]


@pytest.mark.parametrize("orchestration,dataset,result_schedule", valid_configs)
def test_valid(orchestration, dataset, result_schedule):
    loader = V1ConfigurationLoader("", {}, "json")
    with mock_config_open(orchestration=orchestration, dataset=dataset):
        loader.orchestration_loader.validate()
        assert get_orchestration_schedule(loader.get_orchestration("o0")) == result_schedule


invalid_config = [
    (
        """[{"id":"o0","tasks":[{"id":"t0","type":"dummy"}], "schedule": {"datasets": []}}]""",
        """["dataset1", "dataset3"]""",
    ),
    (
        """[{"id":"o0","tasks":[{"id":"t0","type":"dummy"}], "schedule": {"datasets": ["dataset1", "dataset2"]}}]""",
        """["dataset1", "dataset3"]""",
    ),
    (
        """[{"id":"o0","tasks":[{"id":"t0","type":"dummy"}], "schedule": {"datasets": ["data/set1", "dataset2"]}}]""",
        """["data/set1", "dataset2"]""",
    ),
]


@pytest.mark.parametrize("orchestration,dataset", invalid_config)
def test_invalid(orchestration, dataset):
    loader = V1ConfigurationLoader("", {}, "json")
    with mock_config_open(orchestration=orchestration, dataset=dataset):
        with pytest.raises(ConfigurationNotValid):
            loader.orchestration_loader.validate()
