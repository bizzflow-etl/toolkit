import pytest

from tests.utils import mock_config_open
from toolkit.managers.configuration.base import ConfigurationNotValid
from toolkit.managers.configuration.v1.loader import V1ConfigurationLoader

valid_configs = [
    """{"whitelist": {}, "union": {}, "filter": {}, "copy": {}}""",
]


@pytest.mark.parametrize("step", valid_configs)
def test_valid(step):
    loader = V1ConfigurationLoader("", {}, "json")
    with mock_config_open(step=step):
        loader.step_loader.validate()


invalid_configs = [
    """invalid_json""",
    """{}""",
]


@pytest.mark.parametrize("step", invalid_configs)
def test_invalid(step):
    loader = V1ConfigurationLoader("", {}, "json")
    with mock_config_open(step=step):
        with pytest.raises(ConfigurationNotValid):
            loader.step_loader.validate()
