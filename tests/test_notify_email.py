from unittest import mock

from toolkit.dags.helpers.utils import notify_email


@mock.patch("toolkit.dags.helpers.utils.current_config")
@mock.patch("toolkit.dags.helpers.utils.get_notification_level")
@mock.patch("toolkit.dags.helpers.utils.get_notify_param")
@mock.patch("toolkit.dags.helpers.utils.send_email")
def test_notify_email(send_email, _, __, current_config):
    task_instance = mock.Mock()
    dag = mock.Mock()
    dag._dag_id = "dag_id"
    task = mock.Mock()
    task.task_id = "task_id"
    current_config.notification_emails = ["tech-team@bizztreat.com"]
    notify_email({"task_instance": task_instance, "dag": dag, "task": task})
    send_email.assert_called()
