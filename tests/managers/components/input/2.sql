-- sql-parser: sql-fluff

CREATE OR REPLACE TABLE `tr`.out_prvni_den_v_mesici AS
SELECT 1 as vysledek;
-- some comment
if exists (select *
           from (select current_date()) -- some comment
           where extract(day from current_date()) = 1)
then
    -- some comment
    CREATE OR REPLACE TABLE `tr`.out_prvni_den_v_mesici AS
        --some comment
    SELECT 1 as vysledek;
else
    CREATE OR REPLACE TABLE `tr`.out_neni_prvni_den_v_mesici AS
        -- some comment
    SELECT 1 as vysledek;
End if
;