from unittest import mock

import pytest

from toolkit.managers.component.sql import LocalSQLTransformationComponentManager
from toolkit.utils.parsers.none import NoneParser
from toolkit.utils.parsers.sqlfluff import SQLFluffParser
from toolkit.utils.parsers.sqlparse import SQLParseParser

sqls = [
    {
        "input": "tests/managers/components/input/1.sql",
        "output": ["tests/managers/components/output/1_1.sql", "tests/managers/components/output/1_2.sql"],
        "parser": SQLParseParser,
    },
    {
        "input": "tests/managers/components/input/1_a.sql",
        "output": ["tests/managers/components/output/1_1.sql", "tests/managers/components/output/1_2.sql"],
        "parser": SQLParseParser,
    },
    {
        "input": "tests/managers/components/input/1_b.sql",
        "output": ["tests/managers/components/output/1_1.sql", "tests/managers/components/output/1_2.sql"],
        "parser": SQLFluffParser,
    },
    {
        "input": "tests/managers/components/input/2.sql",
        "output": ["tests/managers/components/output/2_1.sql", "tests/managers/components/output/2_2.sql"],
        "parser": SQLFluffParser,
    },
    {
        "input": "tests/managers/components/input/3.sql",
        "output": ["tests/managers/components/output/3.sql"],
        "parser": NoneParser,
    },
    {"input": "tests/managers/components/input/4.sql", "output": [], "parser": SQLParseParser},
    {"input": "tests/managers/components/input/4_a.sql", "output": [], "parser": SQLFluffParser},
]


@pytest.mark.parametrize("sql", sqls)
def test_select_parser(sql):
    component_manager = LocalSQLTransformationComponentManager(
        "test_path",
        "test_id",
        "test_name",
        "bigquery",
        0,
    )
    assert isinstance(component_manager.get_parser(open(sql["input"]).read()), sql["parser"])


@pytest.mark.parametrize("sql", sqls)
def test_sqls_parsing(sql):
    raw_sql = open(sql["input"]).read()
    with mock.patch.object(LocalSQLTransformationComponentManager, "get_raw_sql_queries", return_value=[raw_sql]):
        component_manager = LocalSQLTransformationComponentManager(
            "test_path",
            "test_id",
            "test_name",
            "bigquery",
            0,
        )
        parsed_sql = list(component_manager.get_queries())

    output = []
    for file in sql["output"]:
        output.append(open(file).read())
    assert len(parsed_sql) == len(output)
    for parsed, expected in zip(parsed_sql, output):
        assert parsed == expected
