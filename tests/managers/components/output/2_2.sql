if exists (select *
           from (select current_date())
           where extract(day from current_date()) = 1)
then
    CREATE OR REPLACE TABLE `tr`.out_prvni_den_v_mesici AS
    SELECT 1 as vysledek;
else
    CREATE OR REPLACE TABLE `tr`.out_neni_prvni_den_v_mesici AS
    SELECT 1 as vysledek;
End if;