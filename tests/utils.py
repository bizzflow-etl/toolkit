import os
from unittest import mock


class mock_config_open:
    def __init__(self, **mock_files):
        self.mocks = [
            mock.patch("builtins.open", self.mock_config_file_open(**mock_files)),
            mock.patch.object(os.path, "exists", return_value=True),
        ]

    def __enter__(self):
        for m in self.mocks:
            m.__enter__()

    def __exit__(self, type, value, traceback):
        for m in self.mocks:
            m.__exit__(type, value, traceback)

    def mock_config_file_open(self, **mock_files):
        origin_open = open

        def wrapper(file_name, *args, **kwargs):
            if ".schema.json" in str(file_name):
                return origin_open(file_name, *args, **kwargs)
            for mock_file_name, mock_data in mock_files.items():
                if file_name.startswith(mock_file_name):
                    return mock.mock_open(read_data=mock_data)(file_name, *args, **kwargs)
            return mock.mock_open()(file_name, *args, **kwargs)

        return wrapper
