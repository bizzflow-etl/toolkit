from unittest import mock
from unittest.mock import Mock

import pytest

from toolkit.base.kex import Kex
from toolkit.base.table import Table

table_instance = Table("table_name", kex=Mock(spec=Kex))
parameters = [table_instance, "kex_name.table_name", "project_name.kex_name.table_name"]


@pytest.mark.parametrize("parameter", parameters)
@mock.patch("toolkit.base.table.Kex", autospec=True)
def test_table_from_str(_, parameter):
    assert isinstance(Table.table_from_str(parameter), Table)


invalid_parameters = [Mock(), "no_dot", "more.than.two.dots"]


@pytest.mark.parametrize("parameter", invalid_parameters)
def test_table_from_str_fail(parameter):
    with pytest.raises(ValueError):
        Table.table_from_str(parameter)
