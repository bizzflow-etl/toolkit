import pytest

from toolkit.managers.storage.base import BaseStorageManager

conflicting_inputs = [["table1", "table1"]]


@pytest.mark.parametrize("inputs", conflicting_inputs)
def test_raise_if_duplicates(inputs):
    with pytest.raises(ValueError):
        BaseStorageManager.raise_if_duplicates(inputs)
