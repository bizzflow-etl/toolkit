from unittest import mock

import pytest

from toolkit.base import Step
from toolkit.managers.configuration.v1.loader import V1ConfigurationLoader
from toolkit.managers.configuration.v1.step import StepLoader

valid_configs = [
    {"union": {}, "whitelist": {}, "filter": {}, "copy": {}},
]


@pytest.mark.parametrize("config", valid_configs)
@mock.patch("toolkit.managers.configuration.base.ConfigurationManager.loader", spec=V1ConfigurationLoader)
def test_step_loader(configuration_loader, config):
    configuration_loader.project_path = "/some/path"
    configuration_loader.config_file_format = "json"
    configuration_loader.project_config = {}
    configuration_loader.load_file = lambda _, __: config
    step_loader = StepLoader(configuration_loader)
    assert isinstance(step_loader.get_step(), Step)
    assert step_loader._step.unions == config["union"]
    assert step_loader._step.whitelists == config["whitelist"]
    assert step_loader._step.filters == config["filter"]
    assert step_loader._step.copies == config["copy"]
