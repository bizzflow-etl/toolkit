from unittest import mock

import pytest

from toolkit import current_config
from toolkit.managers.configuration.v2.loader import V2ConfigurationLoader
from toolkit.managers.sandbox import SandboxManager


@mock.patch("os.path.exists", return_value=False)
def test_no_config(_):
    with pytest.raises(Exception):
        current_config.loader


@mock.patch("os.path.exists", return_value=True)
@mock.patch("toolkit.managers.configuration.loader.BaseConfigurationLoader.load_file", return_value={"version": -1})
def test_unsupported_version(_, __):
    with pytest.raises(Exception):
        current_config.loader


@mock.patch("os.path.exists", return_value=True)
@mock.patch("toolkit.managers.configuration.loader.BaseConfigurationLoader.load_file", return_value={"version": 2})
@mock.patch("toolkit.managers.configuration.base.V2ConfigurationLoader", autospec=True)
def test_version(_, __, ___):
    assert isinstance(current_config.loader, V2ConfigurationLoader)


emails = ["tech-team@bizztreat.com"]


@pytest.mark.parametrize("sandbox_user_email", emails)
@mock.patch("toolkit.managers.configuration.base.ConfigurationManager._get_blank_transformation_executor")
@mock.patch("toolkit.managers.configuration.base.SandboxManager", autospec=True)
def test_get_sandbox_manager(_, __, sandbox_user_email):
    assert isinstance(current_config.get_sandbox_manager(sandbox_user_email), SandboxManager)
