from unittest import mock

import pytest

from toolkit.managers.configuration.base import ConfigurationManager
from toolkit.managers.configuration.v1.loader import V1ConfigurationLoader
from toolkit.managers.configuration.v1.orchestration import OrchestrationLoader

with mock.patch.object(ConfigurationManager, "notification_emails", return_value=[]):
    from toolkit.dags.helpers.base import DagCreator
from toolkit.dags.helpers.orchestrations import OrchestrationTaskGenerator
from toolkit.dags.helpers.pools import PoolManager


@pytest.fixture
def ungrouped_tasks():
    config = {
        "id": "test",
        "tasks": [
            {"id": "0", "type": "dummy"},
            {"id": "1", "type": "dummy"},
            {"id": "2", "type": "dummy"},
            {"id": "3", "type": "dummy"},
        ],
    }
    loader = OrchestrationLoader(V1ConfigurationLoader(".", config, "json"))
    orchestration = loader._init_orchestration_from_config(config)
    return orchestration.tasks


@mock.patch.object(PoolManager, "create_pool_if_not_exists", return_value=None)
def test_base(pool_creator_mock, ungrouped_tasks):
    dag = DagCreator(f"test_orchestration", OrchestrationTaskGenerator(ungrouped_tasks)).create()
    assert len(dag.tasks) == 4
    assert dag.task_ids == ["dummy_0", "dummy_1", "dummy_2", "dummy_3"]
    assert dag.task_dict["dummy_0"].upstream_task_ids == set()
    assert dag.task_dict["dummy_1"].upstream_task_ids == {"dummy_0"}
    assert dag.task_dict["dummy_2"].upstream_task_ids == {"dummy_1"}
    assert dag.task_dict["dummy_3"].upstream_task_ids == {"dummy_2"}


@mock.patch.object(PoolManager, "create_pool_if_not_exists", return_value=None)
def test_continue_on_error(pool_creator_mock, ungrouped_tasks):
    ungrouped_tasks[1][0].continue_on_error = True
    dag = DagCreator(f"test_orchestration", OrchestrationTaskGenerator(ungrouped_tasks)).create()
    assert len(dag.tasks) == 5
    assert dag.task_ids == [
        "dummy_0",
        "dummy_1",
        "dummy_1_continue_on_error",
        "dummy_2",
        "dummy_3",
    ]
    assert dag.task_dict["dummy_0"].upstream_task_ids == set()
    assert dag.task_dict["dummy_1"].upstream_task_ids == {"dummy_0"}
    assert dag.task_dict["dummy_1_continue_on_error"].upstream_task_ids == {"dummy_1"}
    assert dag.task_dict["dummy_2"].upstream_task_ids == {"dummy_0", "dummy_1_continue_on_error"}
    assert dag.task_dict["dummy_3"].upstream_task_ids == {"dummy_2"}


@pytest.fixture
def grouped_tasks():
    config = {
        "id": "test",
        "tasks": [
            {"id": "0", "type": "dummy"},
            {"id": "1", "type": "dummy"},
            {"type": "group", "tasks": [{"id": "2", "type": "dummy"}, {"id": "3", "type": "dummy"}]},
            {"id": "4", "type": "dummy"},
            {"id": "5", "type": "dummy"},
        ],
    }
    loader = OrchestrationLoader(V1ConfigurationLoader(".", config, "json"))
    orchestration = loader._init_orchestration_from_config(config)
    return orchestration.tasks


@mock.patch.object(PoolManager, "create_pool_if_not_exists", return_value=None)
def test_grouping(pool_creator_mock, grouped_tasks):
    dag = DagCreator(f"test_orchestration", OrchestrationTaskGenerator(grouped_tasks)).create()
    assert len(dag.tasks) == 6
    assert dag.task_ids == [
        "dummy_0",
        "dummy_1",
        "dummy_2",
        "dummy_3",
        "dummy_4",
        "dummy_5",
    ]
    assert dag.task_dict["dummy_0"].upstream_task_ids == set()
    assert dag.task_dict["dummy_1"].upstream_task_ids == {"dummy_0"}
    assert dag.task_dict["dummy_2"].upstream_task_ids == {"dummy_1"}
    assert dag.task_dict["dummy_3"].upstream_task_ids == {"dummy_1"}
    assert dag.task_dict["dummy_4"].upstream_task_ids == {"dummy_2", "dummy_3"}
    assert dag.task_dict["dummy_5"].upstream_task_ids == {"dummy_4"}


@mock.patch.object(PoolManager, "create_pool_if_not_exists", return_value=None)
def test_grouping_continue_on_error_before_group(pool_creator_mock, grouped_tasks):
    grouped_tasks[1][0].continue_on_error = True
    dag = DagCreator(f"test_orchestration", OrchestrationTaskGenerator(grouped_tasks)).create()
    assert len(dag.tasks) == 7
    assert dag.task_ids == [
        "dummy_0",
        "dummy_1",
        "dummy_1_continue_on_error",
        "dummy_2",
        "dummy_3",
        "dummy_4",
        "dummy_5",
    ]
    assert dag.task_dict["dummy_0"].upstream_task_ids == set()
    assert dag.task_dict["dummy_1"].upstream_task_ids == {"dummy_0"}
    assert dag.task_dict["dummy_1_continue_on_error"].upstream_task_ids == {"dummy_1"}
    assert dag.task_dict["dummy_2"].upstream_task_ids == {"dummy_1_continue_on_error", "dummy_0"}
    assert dag.task_dict["dummy_3"].upstream_task_ids == {"dummy_1_continue_on_error", "dummy_0"}
    assert dag.task_dict["dummy_4"].upstream_task_ids == {"dummy_2", "dummy_3"}
    assert dag.task_dict["dummy_5"].upstream_task_ids == {"dummy_4"}


@mock.patch.object(PoolManager, "create_pool_if_not_exists", return_value=None)
def test_grouping_continue_on_error_in_group(pool_creator_mock, grouped_tasks):
    grouped_tasks[2][0].continue_on_error = True
    dag = DagCreator(f"test_orchestration", OrchestrationTaskGenerator(grouped_tasks)).create()
    assert len(dag.tasks) == 7
    assert dag.task_ids == [
        "dummy_0",
        "dummy_1",
        "dummy_2",
        "dummy_2_continue_on_error",
        "dummy_3",
        "dummy_4",
        "dummy_5",
    ]
    assert dag.task_dict["dummy_0"].upstream_task_ids == set()
    assert dag.task_dict["dummy_1"].upstream_task_ids == {"dummy_0"}
    assert dag.task_dict["dummy_2"].upstream_task_ids == {"dummy_1"}
    assert dag.task_dict["dummy_2_continue_on_error"].upstream_task_ids == {"dummy_2"}
    assert dag.task_dict["dummy_3"].upstream_task_ids == {"dummy_1"}
    assert dag.task_dict["dummy_4"].upstream_task_ids == {"dummy_2_continue_on_error", "dummy_3"}
    assert dag.task_dict["dummy_5"].upstream_task_ids == {"dummy_4"}


@mock.patch.object(PoolManager, "create_pool_if_not_exists", return_value=None)
def test_grouping_continue_on_error_all_in_group(pool_creator_mock, grouped_tasks):
    grouped_tasks[2][0].continue_on_error = True
    grouped_tasks[2][1].continue_on_error = True
    dag = DagCreator(f"test_orchestration", OrchestrationTaskGenerator(grouped_tasks)).create()
    assert len(dag.tasks) == 8
    assert dag.task_ids == [
        "dummy_0",
        "dummy_1",
        "dummy_2",
        "dummy_2_continue_on_error",
        "dummy_3",
        "dummy_3_continue_on_error",
        "dummy_4",
        "dummy_5",
    ]
    assert dag.task_dict["dummy_0"].upstream_task_ids == set()
    assert dag.task_dict["dummy_1"].upstream_task_ids == {"dummy_0"}
    assert dag.task_dict["dummy_2"].upstream_task_ids == {"dummy_1"}
    assert dag.task_dict["dummy_2_continue_on_error"].upstream_task_ids == {"dummy_2"}
    assert dag.task_dict["dummy_3"].upstream_task_ids == {"dummy_1"}
    assert dag.task_dict["dummy_3_continue_on_error"].upstream_task_ids == {"dummy_3"}
    assert dag.task_dict["dummy_4"].upstream_task_ids == {
        "dummy_2_continue_on_error",
        "dummy_3_continue_on_error",
        "dummy_1",
    }
    assert dag.task_dict["dummy_5"].upstream_task_ids == {"dummy_4"}


@mock.patch.object(PoolManager, "create_pool_if_not_exists", return_value=None)
def test_grouping_continue_on_error_after_group(pool_creator_mock, grouped_tasks):
    grouped_tasks[3][0].continue_on_error = True
    dag = DagCreator(f"test_orchestration", OrchestrationTaskGenerator(grouped_tasks)).create()
    assert len(dag.tasks) == 7
    assert dag.task_ids == [
        "dummy_0",
        "dummy_1",
        "dummy_2",
        "dummy_3",
        "dummy_4",
        "dummy_4_continue_on_error",
        "dummy_5",
    ]
    assert dag.task_dict["dummy_0"].upstream_task_ids == set()
    assert dag.task_dict["dummy_1"].upstream_task_ids == {"dummy_0"}
    assert dag.task_dict["dummy_2"].upstream_task_ids == {"dummy_1"}
    assert dag.task_dict["dummy_3"].upstream_task_ids == {"dummy_1"}
    assert dag.task_dict["dummy_4"].upstream_task_ids == {"dummy_2", "dummy_3"}
    assert dag.task_dict["dummy_4_continue_on_error"].upstream_task_ids == {"dummy_4"}
    assert dag.task_dict["dummy_5"].upstream_task_ids == {"dummy_4_continue_on_error", "dummy_2", "dummy_3"}
