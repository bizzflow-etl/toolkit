from datetime import timedelta
from unittest import mock

from toolkit.airflow_plugin.plugin import ExternalDAGSensor
from toolkit.dags.helpers.base import DagCreator
from toolkit.dags.helpers.orchestrations import OrchestrationTaskGenerator
from toolkit.dags.helpers.pools import PoolManager
from toolkit.managers.configuration.v1.loader import V1ConfigurationLoader
from toolkit.managers.configuration.v1.orchestration import OrchestrationLoader


@mock.patch.object(PoolManager, "create_pool_if_not_exists", return_value=None)
def test_sub_orchestration(pool_creator_mock):
    config = {
        "id": "test",
        "tasks": [{"id": "0", "type": "dummy"}, {"id": "sub_orch", "type": "orchestration", "data_age": 50}],
    }
    loader = OrchestrationLoader(V1ConfigurationLoader(".", config, "json"))
    orchestration = loader._init_orchestration_from_config(config)

    dag = DagCreator(f"test_orchestration", OrchestrationTaskGenerator(orchestration.tasks)).create()
    assert len(dag.tasks) == 2
    assert dag.task_ids == ["dummy_0", "required_sub_orch"]
    assert issubclass(dag.task_dict["required_sub_orch"].operator_class, ExternalDAGSensor)
    assert dag.task_dict["required_sub_orch"].external_dag_id == "00_Orchestration_sub_orch"
    assert dag.task_dict["required_sub_orch"].data_age == timedelta(seconds=50)


@mock.patch.object(PoolManager, "create_pool_if_not_exists", return_value=None)
def test_sub_dag(pool_creator_mock):
    config = {
        "id": "test",
        "tasks": [{"id": "0", "type": "dummy"}, {"id": "my_dag", "type": "dag", "data_age": 87}],
    }
    loader = OrchestrationLoader(V1ConfigurationLoader(".", config, "json"))
    orchestration = loader._init_orchestration_from_config(config)

    dag = DagCreator(f"test_orchestration", OrchestrationTaskGenerator(orchestration.tasks)).create()
    assert len(dag.tasks) == 2
    assert dag.task_ids == ["dummy_0", "required_my_dag"]
    assert issubclass(dag.task_dict["required_my_dag"].operator_class, ExternalDAGSensor)
    assert dag.task_dict["required_my_dag"].external_dag_id == "my_dag"
    assert dag.task_dict["required_my_dag"].data_age == timedelta(seconds=87)
