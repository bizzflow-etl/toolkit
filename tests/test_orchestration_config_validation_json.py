import pytest

from tests.utils import mock_config_open
from toolkit.managers.configuration.base import ConfigurationNotValid
from toolkit.managers.configuration.v1.loader import V1ConfigurationLoader

valid_configs = [
    """[{"id":"o0","tasks":[{"id":"t0","type":"dummy"},{"id":"t1","type":"dummy"},{"id":"t2","type":"dummy"}]}]""",
    """[{"id":"o0","tasks":[{"id":"t0","type":"dummy"},{"type":"group","tasks":[{"id":"t1","type":"dummy"},{"id":"t2","type":"dummy"}]}]}]""",
    """[{"id":"o0","tasks":[{"id":"o1","type":"orchestration","data_age":5}]}, {"id":"o1","tasks":[{"id":"t0","type":"dummy"}]}]""",
    """[{"id":"o0","tasks":[{"id":"dag0","type":"dag","data_age":5}]}]""",
]


@pytest.mark.parametrize("config", valid_configs)
def test_valid(config):
    loader = V1ConfigurationLoader("", {}, "json")
    with mock_config_open(orchestration=config):
        loader.orchestration_loader.validate()


invalid_configs = [
    """invalid_json""",
    """[{"id":"o0","tasks":[{"id":"t0","type":"unknown_task_type"}]}]""",
    """[{"id":"o0","tasks":[{"id":"t0","type":"dummy"},{"type":"group","tasks":[]}]}]""",
    """[{"id":"o0","tasks":[{"id":"t0","type":"dummy"},{"type":"group","tasks":[{"type":"group","tasks":[{"id":"t0","type":"dummy"}]}]}]}]""",
    """[{"id":"o0","tasks":[{"id":"t0","type":"orchestration"}]}]""",
    """[{"id":"o0","tasks":[{"id":"t0","type":"orchestration","data_age":5}]}]""",
    """[{"id":"o0","tasks":[{"id":"o0","type":"orchestration","data_age":5}]}]""",
    """[{"id":"o0","tasks":[{"id":"o1","type":"orchestration","data_age":5}]}, {"id":"o1","tasks":[{"id":"o2","type":"orchestration","data_age":5}]}, {"id":"o2","tasks":[{"id":"o0","type":"orchestration","data_age":5}]}]""",
    """[{"id":"o0","tasks":[{"id":"t0","type":"extractor"},{"id":"t1","type":"dummy"},{"id":"t2","type":"dummy"}]}]""",
    """[{"id":"o0","tasks":[{"id":"dag0","type":"dag"}]}]""",
    """[{"id":"o0","tasks":[{"id":"dag0","type":"unknown", "data_age":5}]}]""",
]


@pytest.mark.parametrize("config", invalid_configs)
def test_invalid(config):
    loader = V1ConfigurationLoader("", {}, "json")
    with mock_config_open(orchestration=config):
        with pytest.raises(ConfigurationNotValid):
            loader.orchestration_loader.validate()
