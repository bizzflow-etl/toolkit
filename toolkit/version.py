"""Get toolkit version from package metadata"""

import importlib.metadata

__version__ = importlib.metadata.version("bizzflow-toolkit")
