"""Azure SQL connector"""

import logging

from toolkit.utils.odbc_connector import PyODBCSQLConnector

logger = logging.getLogger(__name__)


class AzureSQLConnector(PyODBCSQLConnector):
    """Azure SQL ODBC Connector"""

    driver = "{ODBC Driver 18 for SQL Server}"
    default_port = 1433
    connection_string_format = (
        "Driver={driver};Server=tcp:{host},{port};Database={database};Uid={username};"
        "Pwd={password};Encrypt=yes;TrustServerCertificate=no;Connection Timeout={timeout};"
    )
