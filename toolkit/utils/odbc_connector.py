"""ODBC Connector using PyODBC"""

import logging

import backoff

logger = logging.getLogger(__name__)

try:
    import aioodbc
    import pyodbc
except ImportError:
    logging.info("aioodbc/pyodbc not installed. ODBC connector not enabled.")


class PyODBCSQLConnector:
    """Base ODBC connector"""

    driver: str = NotImplemented
    default_port: int = NotImplemented
    connection_string_format: str = NotImplemented

    def __init__(self, host, database, username, password, timeout=30, port=None, max_pool_size=10):
        port = port or self.default_port
        self.connection_string = self._get_connection_string(host, port, database, username, password, timeout)
        self._pool = None
        self.username = username
        self.max_pool_size = max_pool_size

    def _get_connection_string(self, host, port, database, username, password, timeout, **kwargs):
        """Generate connection string based on format"""
        if self.connection_string_format is NotImplemented:
            raise NotImplementedError(f"{self.__class__.__name__} does not implement connection")
        return self.connection_string_format.format(
            driver=self.driver,
            host=host,
            port=port,
            database=database,
            username=username,
            password=password,
            timeout=timeout,
            **kwargs,
        )

    @backoff.on_exception(backoff.expo, pyodbc.Error, max_time=300)
    async def _get_pool(self):
        if self._pool is None:
            self._pool = await aioodbc.create_pool(
                dsn=self.connection_string, autocommit=True, minsize=1, maxsize=self.max_pool_size
            )
        return self._pool

    async def executemany(self, query, data):
        """Execute many queries"""
        logger.debug('Execute many queries "%s"', query)
        pool = await self._get_pool()
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                cur.fast_executemany = True
                await cur.executemany(query, data)

    async def execute(self, query, log_query=True):
        """Execute query and return result as list of dicts"""
        return [line async for line in self.execute_generator(query, log_query)]

    async def execute_generator(self, query, log_query=True):
        """Execute and return result as list (or generator) of dicts"""
        logger.debug('Execute query "%s"', query)
        pool = await self._get_pool()
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                try:
                    await cur.execute(query)
                except Exception:
                    if log_query:
                        logger.info("An error occurred when executing query. '''%s'''", query)
                    else:
                        logger.info(
                            "An error occurred when executing query. Query is not logged because it contains sensitive data."
                        )
                    raise

                # non-select query
                if cur.description is None:
                    return
                columns = [info[0] for info in cur.description]
                async for line in cur:
                    yield {column: line[cid] for cid, column in enumerate(columns)}

    async def close(self):
        """Close connection pool"""
        if self._pool:
            logger.info("Closing odbc connection pool, for user %s", self.username)
            self._pool.close()
            await self._pool.wait_closed()
