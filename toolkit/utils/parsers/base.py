import logging
from collections.abc import Iterable

logger = logging.getLogger(__name__)


class BaseParser:
    def __init__(self, dialect: str):
        self.dialect = dialect
        logger.info("Using %s parser for sql", self.__class__.__name__)

    def parse_sql(self, sql: str) -> Iterable[str]:
        raise NotImplementedError
