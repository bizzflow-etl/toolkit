from collections.abc import Iterable

import sqlparse

from toolkit.utils.parsers.base import BaseParser


class SQLParseParser(BaseParser):
    def parse_sql(self, sql: str) -> Iterable[str]:
        query_list_raw = sqlparse.format(sql, strip_comments=True)
        query_list = sqlparse.parse(query_list_raw)
        for query in query_list:
            yield query.value.strip()
