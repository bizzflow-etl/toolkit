from collections.abc import Iterable

from toolkit.utils.parsers.base import BaseParser


class NoneParser(BaseParser):
    def parse_sql(self, sql: str) -> Iterable[str]:
        return [sql]
