import logging
from collections.abc import Iterable

import sqlfluff

from toolkit.utils.parsers.base import BaseParser

logger = logging.getLogger(__name__)

logging.getLogger("sqlfluff.parser").setLevel(logging.WARNING)
logging.getLogger("sqlfluff.linter").setLevel(logging.WARNING)


class SQLFluffParser(BaseParser):
    def parse_sql(self, sql: str) -> Iterable[str]:
        parsed_sql = sqlfluff.parse(sql, dialect=self.dialect)
        parsed_file = parsed_sql["file"]
        if not isinstance(parsed_file, list):
            parsed_file = [parsed_file]
        for statements in parsed_file:
            yield from self._generate_sql_statements_for_parsed_statements_sql(statements)

    def _generate_sql_statements_for_parsed_statements_sql(self, statements) -> Iterable[str]:
        for statement_type, parsed_statement in statements.items():
            # batch is a list of statements - but if it's a single statement, it's not a list
            if statement_type == "batch" and isinstance(parsed_statement, list):
                for in_batch_statements in parsed_statement:
                    yield from self._generate_sql_statements_for_parsed_statements_sql(in_batch_statements)
            elif statement_type in ("multi_statement_segment", "statement", "batch"):
                sql_statement = self._join_parsed_statement(parsed_statement)
                sql_statement = self.format_sql(sql_statement)
                yield sql_statement
            elif statement_type in ("inline_comment", "newline", "statement_terminator"):
                continue
            else:
                raise ValueError(f"Unknown statement type: {statement_type}, {parsed_statement}")

    def _join_parsed_statement(self, parsed_sql: dict) -> str:
        sql_string = ""
        if isinstance(parsed_sql, list):
            for statement in parsed_sql:
                sql_string += self._join_parsed_statement(statement)
        elif isinstance(parsed_sql, dict):
            for statement_type, statement in parsed_sql.items():
                if statement_type in ["inline_comment"]:
                    continue
                else:
                    sql_string += self._join_parsed_statement(statement)
        elif isinstance(parsed_sql, str):
            return parsed_sql
        else:
            raise ValueError(f"Unknown parsed sql: {parsed_sql}")
        return sql_string

    @classmethod
    def format_sql(cls, sql_string: str) -> str:
        lines = sql_string.splitlines()
        result_lines = []
        for line in lines:
            line_rstrip = line.rstrip()
            if line_rstrip:  # if not empty
                result_lines.append(line_rstrip)
        formatted = "\n".join(result_lines)
        if formatted.endswith(";"):
            return formatted
        return formatted + ";"
