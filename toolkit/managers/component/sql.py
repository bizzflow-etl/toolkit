import logging
import os
from collections.abc import Iterable
from glob import glob
from typing import Optional

from toolkit.managers.component.base import BaseComponentManager
from toolkit.utils.parsers.none import NoneParser
from toolkit.utils.parsers.sqlfluff import SQLFluffParser
from toolkit.utils.parsers.sqlparse import SQLParseParser

logger = logging.getLogger(__name__)


class SQLTransformationComponentManager(BaseComponentManager):
    ALLOWED_PARSERS = {
        "none": NoneParser,
        "sql-parse": SQLParseParser,
        "sql-fluff": SQLFluffParser,
    }

    def __init__(
        self,
        component_id: str,
        component_name,
        dialect: str,
        query_timeout: int,
        default_parser: Optional[str] = None,
        **component_config,
    ):
        super().__init__("transformation", component_name, component_id, component_config)
        self.dialect = dialect
        self.query_timeout = query_timeout
        self.default_parser = default_parser or "sql-parse"

    def get_raw_sql_queries(self, skip_files: Optional[list] = None) -> Iterable[str]:
        raise NotImplementedError

    def get_queries(self, skip_files: Optional[list] = None) -> Iterable[str]:
        for sql in self.get_raw_sql_queries(skip_files=skip_files):
            parser = self.get_parser(sql)
            for query in parser.parse_sql(sql):
                logger.info("sql query: %s", query)
                yield query

    def get_parser(self, sql):
        parser_name = self.default_parser
        for line in sql.splitlines():
            if line.startswith("-- sql-parser:"):
                parser_name = line.replace("-- sql-parser:", "").strip()
                logger.info(
                    "Overriding global parser configuration, default: %s, configured: %s",
                    self.default_parser,
                    parser_name,
                )
                break
        if parser_name not in self.ALLOWED_PARSERS:
            logger.warning("Unknown sql-parser: %s, using default %s", parser_name, self.default_parser)
            parser_name = self.default_parser
        return self.ALLOWED_PARSERS.get(parser_name, SQLParseParser)(self.dialect)


class BlankTransformationSQLComponentManager(SQLTransformationComponentManager):
    def __init__(self):
        super().__init__("blank_sql", "blank_sql", "none", 0)

    def get_raw_sql_queries(self, skip_files: Optional[list] = None) -> Iterable[str]:
        logger.info("No sql quires")
        return []


class LocalSQLTransformationComponentManager(SQLTransformationComponentManager):
    def __init__(
        self,
        sql_folder_path: str,
        transformation_id: str,
        transformation_name,
        dialect: str,
        query_timeout: int,
        default_parser: Optional[str] = None,
        **component_config,
    ):
        super().__init__(
            transformation_id, transformation_name, dialect, query_timeout, default_parser, **component_config
        )
        self.sql_folder_path = sql_folder_path

    def get_raw_sql_queries(self, skip_files: Optional[list] = None):
        skip_files = skip_files or []

        search_path = os.path.join(self.sql_folder_path, "*.sql")
        logger.info("SQL files should be in %s", search_path)
        logger.debug("skip_files: %s", skip_files)
        for file_name in sorted(glob(search_path)):
            logger.info(f"Processing queries from {os.path.split(file_name)[1]}")
            if os.path.split(file_name)[1] in skip_files:
                logger.warning(f"Skipping {file_name}")
                continue
            with open(file_name, "r", encoding="utf-8") as fid:
                logger.info("reading script: %s", file_name)
                yield fid.read()
            logger.info("script: %s successfully done!", file_name)
