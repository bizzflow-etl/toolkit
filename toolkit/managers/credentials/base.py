"""Class for credentilas management.
Parent class for various platform credentials managers, e.g. GcpCredentialsManager.
"""

import logging

from toolkit.base.kex import Kex
from toolkit.base.table import Table

logger = logging.getLogger(__name__)


class BaseCredentialsManager:
    """Class for credentilas management.
    Parent class for various platform credentials managers, e.g. GcpCredentialsManager.

    Raises:
        NotImplementedError: If any of methods is not imlemented in the child class.
    """

    async def create_kex_user(self, kex: Kex, user_name: str):
        raise NotImplementedError

    async def grant_kex_permission_to_user(self, kex: Kex, user_name: str, read_only: bool = False):
        raise NotImplementedError

    async def share_table_to_user(self, user_name: str, table: Table):
        """Share table based on sharing configuration"""
        raise NotImplementedError(f"{self.__class__.__name__} does not implement share_table_to_user")

    async def create_sharing_user(self, user_name: str):
        """Share table based on sharing configuration"""
        raise NotImplementedError(f"{self.__class__.__name__} does not implement create_sharing_user")

    def get_user_credentials(self, user_name: str):
        raise NotImplementedError

    async def delete_user(self, user_name: str):
        raise NotImplementedError

    async def user_exists(self, user_name: str) -> bool:
        raise NotImplementedError

    async def setup_user_for_kex(self, kex: Kex, user_name: str, read_only=False):
        if await self.user_exists(user_name):
            logger.info("User for kex '%s' already exists", kex.kex)
        else:
            logger.info("Creating user for kex '%s'", kex.kex)
            await self.create_kex_user(kex, user_name)
        await self.grant_kex_permission_to_user(kex, user_name, read_only)

    async def close(self):
        """Close storage manager for example close pool"""
        return
