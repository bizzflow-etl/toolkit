import os
from logging import getLogger
from typing import TYPE_CHECKING

from toolkit.managers.configuration.validators import BaseConfigValidator

if TYPE_CHECKING:
    from toolkit.managers.configuration.loader import BaseConfigurationLoader

logger = getLogger(__name__)


class BaseDatasetLoader:
    validator: BaseConfigValidator = NotImplemented

    def __init__(self, project_loader: "BaseConfigurationLoader"):
        self.project_loader = project_loader
        self._datasets = None
        self.project_path = self.project_loader.project_path
        self.config_file_format = self.project_loader.config_file_format

    def _load_datasets_file(self) -> dict:
        logger.info("Creating list of Airflow datasets")
        path = os.path.join(self.project_path, f"datasets.{self.config_file_format}")
        try:
            config = self.project_loader.load_file(self.config_file_format, path) or []
        except FileNotFoundError:
            config = []
        self.validator.validate(config)
        datasets = {}
        for dataset_name in config:
            datasets[dataset_name] = self.get_dataset_path(dataset_name)
        return datasets

    def get_datasets(self):
        if self._datasets is None:
            self._datasets = self._load_datasets_file()
        return self._datasets

    @staticmethod
    def get_dataset_path(dataset_name):
        return f"bizzflow://{dataset_name}"

    def validate(self):
        # just access datasets validation is included
        self.get_datasets()
