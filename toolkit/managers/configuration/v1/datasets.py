from toolkit.managers.configuration.datasets import BaseDatasetLoader
from toolkit.managers.configuration.v1.validators import DatasetsValidator


class DatasetLoader(BaseDatasetLoader):
    validator = DatasetsValidator()
