from toolkit.managers.configuration.notifications import BaseNotificationLoader
from toolkit.managers.configuration.v1.validators import NotificationsValidator


class NotificationLoader(BaseNotificationLoader):
    validator = NotificationsValidator()
