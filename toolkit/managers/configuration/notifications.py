import os
from logging import getLogger
from typing import TYPE_CHECKING

from toolkit.executors.notification.http import HttpNotificationExecutor
from toolkit.executors.notification.trigger_dag import BaseAuthTriggerDagExecutor
from toolkit.managers.configuration.utils import reveal_credentials
from toolkit.managers.configuration.validators import BaseConfigValidator
from toolkit.managers.storage.azure_sql import AzureSQLSharedSourceConfig, AzureSQLSharingConfig

if TYPE_CHECKING:
    from toolkit.managers.configuration.loader import BaseConfigurationLoader

logger = getLogger(__name__)


class BaseNotificationLoader:
    validator: BaseConfigValidator = NotImplemented

    def __init__(self, project_loader: "BaseConfigurationLoader"):
        self.project_loader = project_loader
        self._notifications = None
        self._destinations = None
        self.project_path = self.project_loader.project_path
        self.config_file_format = self.project_loader.config_file_format
        self.project_config = self.project_loader.project_config
        self._is_loaded = False

    def _load_notifications_file(self):
        logger.info("Creating list of notifications")
        path = os.path.join(self.project_path, f"notifications.{self.config_file_format}")
        try:
            config = self.project_loader.load_file(self.config_file_format, path) or {}
        except FileNotFoundError:
            config = {}
        self.validator.validate(config)
        self._notifications = config
        self._is_loaded = True

    def get_notification_executor(self, notification_id):
        if not self._is_loaded:
            self._load_notifications_file()
        config = self._notifications[notification_id]
        vault_manager = self.project_loader.get_vault_manager()
        config = reveal_credentials(config, vault_manager)
        notification_type = config.pop("type")
        retry_count = config.pop("retry_count", 0)
        if notification_type == "http":
            url = config.pop("url")
            method = config.pop("method")
            return HttpNotificationExecutor(url=url, method=method, retry_count=retry_count, **config)
        elif notification_type == "trigger-airflow-dag":
            return BaseAuthTriggerDagExecutor(
                base_url=config["base_url"],
                dag_id=config["dag_id"],
                username=config["username"],
                password=config["password"],
                retry_count=retry_count,
            )

    def get_notifications_ids(self):
        if not self._is_loaded:
            self._load_notifications_file()
        return self._notifications.keys()

    def validate(self):
        # just access notifications_ids validation is included
        self.get_notifications_ids()  # noqa
