class ConfigurationNotValid(Exception):
    pass


class UnsupportedConfigurationVersion(ConfigurationNotValid):
    pass
