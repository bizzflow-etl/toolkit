from toolkit import metaclasses
from toolkit.managers.configuration.base import ConfigurationManager


class CurrentConfigurationManager(ConfigurationManager, metaclass=metaclasses.Singleton):
    pass


# It just init class but all load are lazy
current_config = CurrentConfigurationManager()
