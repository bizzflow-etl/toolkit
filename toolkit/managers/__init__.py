"""Init module for *managers* toolkit's submodule.

*Managers* contain classes that manage project's metadata and control project's resources.
"""
