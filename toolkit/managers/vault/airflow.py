"""Class for Airflow credentials management.
"""

import datetime
import json
import logging
from typing import List

from airflow.models import Connection
from airflow.settings import Session

from toolkit.managers.vault.base import BaseVaultManager

logger = logging.getLogger(__name__)


class AirflowVaultManager(BaseVaultManager):
    """Class for Airflow credentials management."""

    def get_credentials(self, key: str):
        """Get password for given Airflow Connection.

        Arguments:
            key {str} -- Airflow connection id (conn_id)

        Reutrns:
            {str} -- Airflow connection password (password)
        """
        session = Session()
        connections = list(session.query(Connection).filter(Connection.conn_id == key))
        if not connections:
            return None
        connection = connections[0]
        logger.info("Working with connection: %s", connection.conn_id)
        password = connection.password
        session.close()
        return password

    def list_credentials(self) -> List[str]:
        """Get list of credential keys available in the vault"""
        session = Session()
        connections = session.query(Connection).all()
        return [conn.conn_id for conn in connections]

    def store_credentials(self, key: str, value: str | None):
        """Store credentials to Airflow.
        Store credentials to Airflow, save current timestamp as connection.extra.
        If connection already exists, value is updated. If value is None, existing key is removed.

        Arguments:
            key {str} -- Airflow connection id (conn_id)

            value {str | None} -- Airflow connection password (password). If None, existing value is removed.
        """

        now = datetime.datetime.now()
        session = Session()
        clist = list(session.query(Connection).filter(Connection.conn_id == key))
        if clist:
            connection = clist[0]
            logger.info("Connection id: '%s' already exists, updating value.", connection.conn_id)
            # Remove key if value is None
            if value is None:
                logger.info("Deleting connection id '%s'", connection.conn_id)
                session.delete(connection)
                session.commit()
                session.close()
                return
            connection.password = value
            try:
                extra = json.loads(connection.extra)
            except (json.JSONDecodeError, TypeError):
                extra = {}
            extra["updated_at"] = now.isoformat()
            connection.extra = json.dumps(extra)
        else:
            extra = {
                "created_timestamp": now.timestamp(),
                "created_at": now.isoformat(),
                "updated_at": now.isoformat(),
            }
            connection = Connection(conn_id=key, conn_type="bizzflow_vault", password=value, extra=json.dumps(extra))
        session.add(connection)
        session.commit()
        logger.info("Updated connection: %s", connection.conn_id)
        session.close()

    def delete_credential(self, key: str):
        self.store_credentials(key, None)

    @classmethod
    def build_default(cls):
        return AirflowVaultManager()
