"""Worker Manager

Module for managing all worker managers on different platforms.
"""

import os
import socket
from logging import getLogger
from typing import TextIO, Union

import backoff
import invoke.exceptions
import paramiko
import paramiko.ssh_exception
from fabric import Connection as SSH

logger = getLogger(__name__)


class InvalidSSHProtocolBanner(Exception):
    """Catching specific SSH error from paramiko library"""


class InvalidSSHKeyError(Exception):
    """Invalid SSH key (we may not be needing any key at all)"""


class RetriedSSH(SSH):
    """SSH allowing to catch SSH Banner Protocol Error"""

    def run(self, command, **kwargs):
        try:
            return super().run(command, **kwargs)
        except paramiko.ssh_exception.SSHException as error:
            if str(error).startswith("Error reading SSH protocol banner"):
                raise InvalidSSHProtocolBanner("Invalid SSH protocol banner") from error
            raise
        except ValueError as error:
            if str(error).startswith("q must be exactly"):
                raise InvalidSSHKeyError(str(error)) from error
            raise


def _get_ssh_connection(host, user) -> RetriedSSH:
    # This looks ugly when in the base class due to RetryManager
    # forcing us to indent the code by three levels
    if os.path.exists("/home/bizzflow/.ssh/id_rsa"):
        ssh = RetriedSSH(
            host=host,
            user=user,
            connect_kwargs={"key_filename": "/home/bizzflow/.ssh/id_rsa"},
        )
        try:
            ssh.run("whoami")
            return ssh
        except (paramiko.SSHException, InvalidSSHKeyError):
            logger.error("Failed to authenticate using SSH key, retrying with password", exc_info=True)
    ssh = RetriedSSH(host=host, user=user, connect_kwargs={"password": ""})
    try:
        ssh.run("whoami")
        return ssh
    except paramiko.SSHException:
        logger.error("Password authentication failed, no more authentication methods to try")
        raise


class BaseWorkerManager:
    """Abstract class for all worker managers on different platforms, e.g. GcpWorkerManager.

    Raises:
        NotImplementedError: If any of methods is not implemented in the child class.
    """

    ssh_errors = (
        paramiko.ssh_exception.NoValidConnectionsError,
        InvalidSSHProtocolBanner,
        TimeoutError,
        socket.timeout,
        EOFError,
    )

    def __init__(self, host, user, data_path, components_path, config_path, keep_running=False):
        """Initiate Worker Manager."""
        self.data_path = data_path
        self.components_path = components_path
        self.config_path = config_path or "~/.config"
        self.keep_running = keep_running

        self._host = host
        self._user = user

        self._ssh = None

    def _disconnect(self, *args, **kwargs):
        self._ssh = None

    @property
    def ssh(self) -> SSH:
        """SSH connection instance"""

        @backoff.on_exception(backoff.expo, self.ssh_errors, max_time=180, on_backoff=self._disconnect)
        def get_ssh_connection():
            return _get_ssh_connection(self._host, self._user)

        if self._ssh is None:
            self._ssh = get_ssh_connection()
        return self._ssh

    def start(self, *args, **kwargs):
        """Start worker."""
        raise NotImplementedError("This method must be overridden")

    def stop(self):
        """Stop worker."""
        raise NotImplementedError("This method must be overridden")

    def get_running(self, *args, **kwargs):
        """Check if worker already running."""
        raise NotImplementedError("This method must be overridden")

    def put(self, source: Union[str, TextIO], destination: str):
        """Put a local file {source} (a file-like object or path) into worker's destination"""
        return self.ssh.put(source, destination, preserve_mode=False)

    def run(self, command, safe_command_for_logging=None, **kwargs):
        """Run command in the worker's shell"""
        # that will be used instead of running directly any command
        safe_command_for_logging = safe_command_for_logging or command

        @backoff.on_exception(
            backoff.expo, self.ssh_errors, max_time=180, on_backoff=self.ensure_virtual_machine_is_running
        )
        def _run_command():
            logger.info(f"Running command '{safe_command_for_logging}' by ssh")
            try:
                return self.ssh.run(command, **kwargs)
            except invoke.exceptions.UnexpectedExit as e:
                e.result.command = safe_command_for_logging
                raise e

        return _run_command()

    @backoff.on_exception(backoff.expo, TimeoutError, max_time=300)
    def ensure_virtual_machine_is_running(self, *args, **kwargs):
        logger.info("Ensuring VM is running...")
        running_worker = self.get_running()
        if running_worker is None:
            logger.warning(
                "Virtual Machine is not in either of acceptable states (stopped nor running), we will try to wait before finding out again."
            )
            raise TimeoutError("Virtual Machine cool-down timed out.")
        elif not running_worker:
            logger.info("Starting VM...")
            self.start()
        logger.info("VM is running")
