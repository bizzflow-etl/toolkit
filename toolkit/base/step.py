"""Step
Module for set of primitive and configurable operations with data.
"""

import uuid
from logging import getLogger
from typing import TYPE_CHECKING, Dict, List, Optional

from toolkit.base.kex import Kex
from toolkit.base.table import Table
from toolkit.managers.credentials.base import BaseCredentialsManager

if TYPE_CHECKING:
    from toolkit.managers.storage.base import BaseStorageManager

logger = getLogger(__name__)


class UnionConf:
    def __init__(self, tables: List[Table], distinct):
        self.tables = tables
        self.distinct = distinct


class WhitelistConf:
    def __init__(self, columns: List[str]):
        self.columns = columns


class FilterColumn:
    def __init__(self, name: str, operator: str, value: str, data_type: Optional[str] = None):
        self.name = name
        self.data_type = data_type
        self.operator = operator
        self.value = value

    def get_filter_query(self, quotation_mark):
        if self.data_type:
            column = f"CAST({quotation_mark}{self.name}{quotation_mark} AS {self.data_type})"
        else:
            column = f"{quotation_mark}{self.name}{quotation_mark}"
        return f"{column} {self.operator} {self.value}"


class FilterConf:
    def __init__(self, custom_query, columns: List[FilterColumn]):
        self.custom_query = custom_query
        self.columns = columns

    def get_filter_query(self, quotation_mark):
        if self.custom_query:
            return self.custom_query
        else:
            return " AND ".join((column.get_filter_query(quotation_mark) for column in self.columns))


class CopyConf:
    def __init__(
        self,
        destination: Optional[Table] = None,
        incremental: Optional[bool] = False,
        primary_keys: Optional[List[str]] = None,
        mark_deletes: Optional[bool] = False,
    ):
        self.destination = destination
        self.incremental = incremental
        self.primary_keys = primary_keys or []
        self.mark_deletes = mark_deletes


class Step:
    """Class for Step.
    Parent class for various Step implementations, e.g. BQStep.

    Raises:
        NotImplementedError: If any of methods is not imlemented in the child class.
    """

    def __init__(
        self,
        storage_manager: "BaseStorageManager",
        credentials_manager: "BaseCredentialsManager",
        unions: Dict[str, UnionConf],
        whitelists: Dict[str, WhitelistConf],
        filters: Dict[str, FilterConf],
        copies: Dict[str, CopyConf],
    ):
        self.storage_manager = storage_manager
        self.credentials_manager = credentials_manager
        self.unions = unions
        self.whitelists = whitelists
        self.filters = filters
        self.copies = copies

    def get_kex(self):
        return Kex(f"tmp_{uuid.uuid4().hex}")

    @staticmethod
    def get_table_id(table):
        project_name = table.project
        kex_name = table.kex.kex
        table_name = table.table
        if kex_name.startswith("tr_"):
            kex_name = "tr"
        return f"{project_name}.{kex_name}.{table_name}"

    def create_tmp_table(self, kex):
        table = Table(uuid.uuid4().hex, kex)
        return table

    async def union_table(self, kex: Kex, table: Table, origin_table_id: str) -> Table:
        """Union multiple source tables to one destination table."""
        try:
            union = self.unions[origin_table_id]
        except KeyError:
            return table
        union_tables = self.replace_tr_for_real_kex_in_union(union.tables, table.kex)
        logger.info("Creating union for table %s", origin_table_id)
        destination = self.create_tmp_table(kex)
        await self.storage_manager.union_table(union_tables, destination, union.distinct)
        return destination

    @staticmethod
    def replace_tr_for_real_kex_in_union(tables, source_kex):
        output_tables = []
        for table in tables:
            if table.kex.get_name(quotation_mark=None) == "tr":
                logger.debug(f'Replacing "tr" for real transformation kex {source_kex.get_name(quotation_mark=None)}')
                output_tables.append(Table(table.get_name(quotation_mark=None), source_kex))
            else:
                output_tables.append(table)
        return output_tables

    async def whitelist_columns(self, kex: Kex, table: Table, origin_table_id: str):
        """Copy only specified columns from source table to destination table."""
        try:
            whitelist = self.whitelists[origin_table_id]
        except KeyError:
            return table
        logger.info("Whitelisting table %s", origin_table_id)
        destination = self.create_tmp_table(kex)
        await self.storage_manager.whitelist_table(table, destination, whitelist.columns)
        return destination

    async def filter_rows(self, kex: Kex, table: Table, origin_table_id: str):
        """Copy only specified rows from source table to destination table."""
        try:
            filter_conf = self.filters[origin_table_id]
        except KeyError:
            return table
        filter_query = filter_conf.get_filter_query(quotation_mark=self.storage_manager.QUOTATION_MARK)
        logger.info("Filtering table %s by %s", origin_table_id, filter_query)
        destination = self.create_tmp_table(kex)
        await self.storage_manager.filter_table(table, destination, filter_query)
        return destination

    async def copy_table(self, table: Table, default_destination: Table, origin_table_id: str):
        """Copy source table to destination table incrementally."""
        try:
            copy = self.copies[origin_table_id]
        except KeyError:
            copy = CopyConf()

        destination = copy.destination or default_destination
        await self.storage_manager.create_kex(destination.kex)

        logger.info(
            f"Table {origin_table_id} will be copied with step configuration (incremental: {copy.incremental}, primary_keys: {copy.primary_keys}, mark_deletes: {copy.mark_deletes}"
        )

        if copy.incremental:
            await self.storage_manager.incremental_copy_table(table, destination, copy.primary_keys, copy.mark_deletes)
        else:
            await self.storage_manager.copy_table(table, destination)
        return destination

    async def process(self, table: Table, default_destination: Table):
        """Execute steps in logical order."""
        kex = self.get_kex()
        origin_table_id = self.get_table_id(table)
        if self.should_be_skipped(origin_table_id):
            logger.info(f"Skipping table {origin_table_id} processing.")
            return
        try:
            await self.storage_manager.create_kex(kex)
            united_table = await self.union_table(kex, table, origin_table_id)
            whitelisted_table = await self.whitelist_columns(kex, united_table, origin_table_id)
            filtered_table = await self.filter_rows(kex, whitelisted_table, origin_table_id)
            destination = await self.copy_table(filtered_table, default_destination, origin_table_id)
            logger.info(f"Processed table {table.get_full_id()} => {destination.get_full_id()}.")
            for destination_project in self.storage_manager.shared_out_table_destinations(destination):
                logger.info(f"Sharing table {destination.table} to project {destination_project}")
                user_name = f"sh_{destination_project}"
                await self.credentials_manager.create_sharing_user(user_name)
                await self.credentials_manager.share_table_to_user(user_name, destination)
        finally:
            await self.storage_manager.delete_kex(kex)
        return destination

    def should_be_skipped(self, origin_table_id):
        try:
            self.copies[origin_table_id]
        except KeyError:
            for key, union in self.unions.items():
                for table in union.tables:
                    if table.get_full_id() == origin_table_id:
                        # table is processed as part of the union, and have no explicit copy definition
                        # so let's consider it as processed and skip it
                        return True
        return False
