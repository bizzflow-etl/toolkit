"""Class for triggering Airflow DAG via REST API.
"""

import logging
import urllib.parse
from datetime import datetime

import pytz
from requests.auth import HTTPBasicAuth

from toolkit.executors.notification.http import HttpNotificationExecutor

logger = logging.getLogger(__name__)


class BaseAuthTriggerDagExecutor(HttpNotificationExecutor):
    """Class for triggering Airflow DAG via REST API."""

    def __init__(self, base_url: str, dag_id: str, username: str, password: str, retry_count=None):
        self.dag_id = dag_id
        path = f"/api/v1/dags/{dag_id}/dagRuns"
        url = urllib.parse.urljoin(base_url, path)
        headers = {"Content-Type": "application/json", "Accept": "application/json"}
        auth = HTTPBasicAuth(username, password)
        now_str = datetime.now(tz=pytz.utc).isoformat()
        data = {
            "dag_run_id": f"bizzflow_notification__{now_str}",
            "logical_date": now_str,
            "conf": {},
        }
        super().__init__(
            url,
            "POST",
            retry_count=retry_count,
            headers=headers,
            auth=auth,
            json=data,
        )
