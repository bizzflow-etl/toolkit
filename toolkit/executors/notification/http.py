"""Class for sending HTTP request.
"""

import logging

import backoff
import requests

from toolkit.executors.base.base import BaseExecutor

logger = logging.getLogger(__name__)


class HttpNotificationExecutor(BaseExecutor):
    """Class for sending HTTP request."""

    def __init__(self, url: str, method: str, retry_count=0, **kwargs):
        super().__init__()
        self.url = url
        self.method = method
        self.retry_count = retry_count
        self.kwargs = kwargs

    async def run(self):
        @backoff.on_exception(backoff.expo, requests.exceptions.RequestException, max_tries=self.retry_count + 1)
        def run_request():
            logger.info("Sending notification")
            response = requests.request(self.method, self.url, **self.kwargs)
            response.raise_for_status()

        run_request()
        logger.info("Notification successfully sent")
