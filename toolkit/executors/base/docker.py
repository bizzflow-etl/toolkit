"""Base docker executor"""

import asyncio
import logging
import os

from toolkit.base import Step, Table
from toolkit.executors.base.base import BaseComponentExecutor, InputMixin, OutputMixin
from toolkit.managers.component.docker import BaseDockerComponentManager
from toolkit.managers.file_storage.base import BaseFileStorageManager
from toolkit.managers.storage.base import BaseStorageManager
from toolkit.managers.worker.base import BaseWorkerManager
from toolkit.utils.stopwatch import stopwatch

logger = logging.getLogger(__name__)


class DockerExecutor(BaseComponentExecutor):
    """Base docker executor"""

    def __init__(
        self,
        worker_manager: BaseWorkerManager,
        storage_manager: BaseStorageManager,
        file_storage_manager: BaseFileStorageManager,
        component_manager: BaseDockerComponentManager,
        step: Step,
    ):
        super().__init__(storage_manager, component_manager, step)
        self.file_storage_manager = file_storage_manager
        self.worker_manager = worker_manager

    async def create_environment(self):
        """Create environment and get input data"""
        with stopwatch("Create environment", __class__.__name__):
            await super().create_environment()
            self.file_storage_manager.clean_up_live_storage()
            self.worker_manager.ensure_virtual_machine_is_running()
            self.component_manager.init_component()
            await self.component_manager.get_docker_image()
            self.component_manager.create_config_dir()
            self.component_manager.load_config_file_to_worker()

    async def run(self, **kwargs):
        """Run docker execution"""
        volumes = " -v ".join((f'"{volume}"' for volume in self.get_docker_volumes()))
        docker_command = f"docker run --rm -v {volumes} {self.component_manager.component_name}"
        with stopwatch(log_msg=docker_command, caller_class=__class__.__name__):
            result = self.worker_manager.run(docker_command, hide=False)
            if not result.ok:
                logger.error("Error occurred during job")
                logger.error(result.stdout)
                logger.error(result.stderr)
                raise Exception("Error occurred during job. See log above for more details.")
            logger.info("Job finished successfully")

    @stopwatch("Clean environment", __qualname__)
    async def clean_environment(self):
        """Clean worker machine Shutdown worker if specified.

        Keyword Arguments:
            shutdown_worker {bool} -- Whether to shutdown worker afterwards (default: {True})
        """
        self.component_manager.clean_component()
        await super().clean_environment()

        self.file_storage_manager.clean_up_live_storage()

    def get_docker_volumes(self):
        volumes = []
        volumes.append(f"{self.component_manager.host_volume_config_path}:/config/")
        return volumes


class DockerInputMixin(InputMixin):
    """A mixin to enable input on component"""

    @stopwatch("Create environment", __qualname__)
    async def create_environment(self):
        """Create environment"""
        await super().create_environment()
        self.create_input_dir()

    def create_input_dir(self):
        """Create input directory for component"""
        result = self.worker_manager.run(f"mkdir -p '{self.component_manager.worker_component_input_path}'", hide=True)
        if not result.ok:
            raise Exception("Could not create component input dir.\n%s\n%s", result.stdout, result.stderr)

    @stopwatch("Process input table", __qualname__)
    async def process_input_table(self, table):
        """Unload input table to worker machine"""
        destination = await super().process_input_table(table)
        if destination:
            logger.info(
                "Unloading table %s -> to worker machine folder %s",
                destination.get_id(),
                self.component_manager.worker_component_input_path,
            )
            await self.storage_manager.export_to_worker(
                destination,
                self.component_manager.worker_component_input_path,
                self.worker_manager,
                self.file_storage_manager,
            )
        else:
            logger.info("No input table to unload, table was skipped in previous process")

    def get_docker_volumes(self) -> list:
        """Get list of docket volumes"""
        volumes = super().get_docker_volumes()
        volumes.append(f"{self.component_manager.host_volume_input_path}:/data/in/tables/")
        return volumes


class DockerOutputMixin(OutputMixin):
    """Mixin to enable output on component"""

    @stopwatch("Create output mapping", __qualname__)
    async def create_output_mapping(self):
        """Create output mapping"""
        await self.file_storage_manager.upload_files_from_worker(
            self.worker_manager, self.component_manager.worker_component_output_path
        )
        await self.load_output_to_storage()
        await super().create_output_mapping()

    async def load_output_to_storage(self):
        """Unload output to the storage"""
        blobs = self.file_storage_manager.list_files("")
        tasks = []
        for blob in blobs:
            logger.info("About to load blob %s", blob)
            basename = os.path.basename(blob)
            table_name = (
                f"{self.output_table_prefix}{self.storage_manager.normalize_string(os.path.splitext(basename)[0])}"
            )
            table = Table(kex=self.working_kex, table_name=table_name)
            tasks.append(self.storage_manager.load_table(table, basename, self.file_storage_manager))
        await asyncio.gather(*tasks)

    @stopwatch("Create docker environment", __qualname__)
    async def create_environment(self):
        """Create environment with output dir"""
        await super().create_environment()
        self.create_output_dir()

    def get_docker_volumes(self) -> list:
        """Get docker volumes list"""
        volumes = super().get_docker_volumes()
        volumes.append(f"{self.component_manager.host_volume_output_path}:/data/out/tables/")
        return volumes

    def create_output_dir(self):
        """Create output directory for component"""
        result = self.worker_manager.run(f"mkdir -p '{self.component_manager.worker_component_output_path}'", hide=True)
        if not result.ok:
            raise Exception("Could not create component output dir.\n{}\n{}".format(result.stdout, result.stderr))


class DockerInputOutputMixin(DockerInputMixin, DockerOutputMixin):
    """Enable both input and output on a component"""
