"""Base executor"""

import asyncio
import logging
import uuid
from typing import Optional

import toolkit
from toolkit.base import Kex, Step, Table
from toolkit.managers.component.base import BaseComponentManager
from toolkit.managers.storage.base import BaseStorageManager
from toolkit.utils.stopwatch import stopwatch

logger = logging.getLogger(__name__)


class BaseExecutor:
    async def run(self, **kwargs):
        """Run execution (this must be overriden)"""
        raise NotImplementedError

    async def pre_run(self):
        """Run pre execution"""
        return

    async def post_run(self):
        """Run post execution"""
        return

    async def clean_up(self):
        """Run clean up"""
        return

    async def execute(self):
        try:
            logger.info("Execute %s, using Bizzflow Toolkit version %s", self.__class__, toolkit.__version__)
            await self.pre_run()
            await self.run()
            await self.post_run()
        except Exception as e:
            logger.error("Unexpected error: %s", e)
            raise
        finally:
            await self.clean_up()


class BaseExecutorWithKex(BaseExecutor):
    should_delete_working_kex = True

    def __init__(self, storage_manager: BaseStorageManager):
        self.storage_manager = storage_manager
        self.working_kex = Kex(self._create_working_kex_name())

    @stopwatch("Create environment/kex", __qualname__)
    async def create_environment(self):
        """Create transitional environment for execution"""
        await self.storage_manager.create_kex(self.working_kex)

    @stopwatch("Clean environment/delete kex", __qualname__)
    async def clean_environment(self):
        """Clean transient environment"""
        if self.should_delete_working_kex:
            await self.storage_manager.delete_kex(self.working_kex)

    async def pre_run(self):
        """Run pre execution - prepare environment"""
        await super().pre_run()
        await self.create_environment()

    async def clean_up(self):
        """Run clean up - clean environment"""
        await super().clean_up()
        await self.clean_environment()
        await self.storage_manager.close()

    def _create_working_kex_name(self):
        return f"tmp_{uuid.uuid4().hex}"


class BaseComponentExecutor(BaseExecutorWithKex):
    """Base executor for component"""

    def __init__(self, storage_manager: BaseStorageManager, component_manager: BaseComponentManager, step: Step):
        self.component_manager = component_manager
        self.step = step
        super().__init__(storage_manager)


class InputMixin:
    async def pre_run(self):
        """Run pre execution - create input mapping"""
        await super().pre_run()
        await self.create_input_mapping()

    @stopwatch("Create input mapping", __qualname__)
    async def create_input_mapping(self, inputs: Optional[list] = None):
        logger.info("Running input mapping")
        inputs = inputs or self.inputs
        in_tables = await self.storage_manager.list_input_tables(inputs)
        logger.info("Input mapping tables: %s", str(in_tables))
        tasks = []
        for table in in_tables:
            task = asyncio.create_task(self.process_input_table(table))
            tasks.append(task)
        await asyncio.gather(*tasks)

    async def process_input_table(self, table):
        logger.info("Processing input for table %s", table.get_id())
        destination_table = Table(table_name="in_{}".format(table.table), kex=self.working_kex)
        return await self.step.process(table=table, default_destination=destination_table)

    @property
    def inputs(self):
        try:
            return self._inputs
        except AttributeError:
            return []


class OutputMixin:
    output_table_prefix = "out_"

    async def post_run(self):
        """Run post execution - create output mapping"""
        await super().post_run()
        await self.create_output_mapping()

    @stopwatch("Create output mapping", __qualname__)
    async def create_output_mapping(self):
        if self.output_kex.get_id() not in [kex.get_id() for kex in await self.storage_manager.list_kexes()]:
            logger.info("Output kex %s does not exist, creating", self.output_kex.get_id())
            await self.storage_manager.create_kex(self.output_kex)
        tasks = []
        for table in await self.storage_manager.list_tables(self.working_kex):
            if table.table.startswith(self.output_table_prefix):
                task = asyncio.create_task(
                    self.process_output_table(table, table.table[len(self.output_table_prefix) :])
                )
                tasks.append(task)
        await asyncio.gather(*tasks)

    async def process_output_table(self, table, output_table_name=None):
        output_table_name = output_table_name or table.table
        with stopwatch(f"Process output table {output_table_name}", __class__.__name__):
            default_output_table = Table(table_name=output_table_name, kex=self.output_kex)
            logger.info(f"Processing output for table {table.table}")
            return await self.step.process(table, default_destination=default_output_table)

    def _create_output_kex_name(self) -> str:
        raise NotImplementedError

    @property
    def output_kex(self):
        try:
            return self._output_kex
        except AttributeError:
            self._output_kex = Kex(self._create_output_kex_name())
        return self._output_kex
