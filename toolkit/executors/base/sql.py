"""Base SQL executor"""

import logging

from toolkit.base import Step
from toolkit.executors.base.base import BaseComponentExecutor, InputMixin, OutputMixin
from toolkit.managers.component.sql import SQLTransformationComponentManager
from toolkit.managers.credentials.base import BaseCredentialsManager
from toolkit.managers.storage.base import BaseStorageManager

logger = logging.getLogger(__name__)


class SQLExecutor(BaseComponentExecutor):
    """Base SQL executor"""

    def __init__(
        self,
        storage_manager: BaseStorageManager,
        component_manager: SQLTransformationComponentManager,
        step: Step,
        credentials_manager: BaseCredentialsManager,
    ):
        super().__init__(storage_manager, component_manager, step)
        self.credentials_manager = credentials_manager
        self.schema_name = self.working_kex.kex

    async def clean_up(self):
        await super().clean_up()
        await self.credentials_manager.close()


class SQLInputMixin(InputMixin):
    """Enable input in sql component"""


class SQLOutputMixin(OutputMixin):
    """Enable output on sql component"""


class SQLInputOutputMixin(SQLInputMixin, SQLOutputMixin):
    """Enable both input and output on sql component"""
