"""Init module for *executors* toolkit's submodule.

*Executors* contain classes to control pipeline's flow in order to actively **run** jobs.
"""
