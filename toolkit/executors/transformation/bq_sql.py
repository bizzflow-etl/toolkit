"""Class for executing transformation in Google BigQuery.
"""

import logging

from google.cloud import bigquery
from google.oauth2 import service_account

from toolkit.base import Step
from toolkit.executors.transformation.base_sql import SqlTransformationExecutor
from toolkit.managers.component.sql import SQLTransformationComponentManager
from toolkit.managers.credentials.gcp import GcpCredentialsManager
from toolkit.managers.storage.bq_sql import BqStorageManager

logger = logging.getLogger(__name__)


class BqSqlTransformationExecutor(SqlTransformationExecutor):
    """Class for executing transformation in Google BigQuery."""

    DIALECT = "bigquery"

    def __init__(
        self,
        storage_manager: BqStorageManager,
        component_manager: SQLTransformationComponentManager,
        step: Step,
        credentials_manager: GcpCredentialsManager,
        inputs: list,
        output: str,
    ):
        super().__init__(storage_manager, component_manager, step, credentials_manager, inputs, output)
        self.transformation_user = (
            self.component_manager.component_config.get("transformation_service_account") or "transformation"
        )
        self._bq_client = None
        self.bq_session_id = None

    @property
    def bq_client(self):
        if not self._bq_client:
            json_key = self.credentials_manager.get_user_credentials(self.transformation_user)
            credentials = service_account.Credentials.from_service_account_info(json_key)
            self._bq_client = bigquery.Client(credentials=credentials, project=json_key["project_id"])
            logger.info("Transformation service account: %s", json_key["client_email"])
        return self._bq_client

    @property
    def job_config(self):
        if self.bq_session_id:
            return bigquery.QueryJobConfig(
                connection_properties=[bigquery.ConnectionProperty("session_id", self.bq_session_id)]
            )
        return bigquery.QueryJobConfig(create_session=True)

    def tr_replace(self, query):
        return query.replace("`tr`", "`{}`".format(self.working_kex.kex))

    async def run_query(self, query):
        query_job = self.bq_client.query(query, job_config=self.job_config)
        try:
            self.bq_session_id = query_job.session_info.session_id
        except AttributeError:
            pass
        return query_job.result(timeout=self.component_manager.query_timeout)
