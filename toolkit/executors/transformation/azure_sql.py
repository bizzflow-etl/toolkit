"""Class for executing transformation in Azure SQL.
"""

import logging

from toolkit.executors.transformation.base_sql import SqlTransformationExecutor
from toolkit.managers.component.sql import SQLTransformationComponentManager
from toolkit.managers.credentials import AzureSQLCredentialManager
from toolkit.managers.storage import AzureSQLStorageManager
from toolkit.utils.azure_sql import AzureSQLConnector
from toolkit.utils.stopwatch import stopwatch

logger = logging.getLogger(__name__)


class AzureSQLTransformationExecutor(SqlTransformationExecutor):
    """Class for executing transformation in Azure SQL."""

    DIALECT = "tsql"

    def __init__(
        self,
        storage_manager: AzureSQLStorageManager,
        component_manager: SQLTransformationComponentManager,
        step,
        credentials_manager: AzureSQLCredentialManager,
        inputs: list,
        output: str,
    ):
        super().__init__(storage_manager, component_manager, step, credentials_manager, inputs, output)
        self._connector = None

    @property
    def connector(self):
        if not self._connector:
            credentials = self.credentials_manager.get_user_credentials(self.transformation_user)
            self._connector = AzureSQLConnector(
                host=credentials["host"],
                database=credentials["database"],
                username=credentials["user"],
                password=credentials["password"],
                timeout=self.component_manager.query_timeout,
                port=credentials["port"],
            )
        return self._connector

    def tr_replace(self, query):
        return query.replace('"tr"', f'"{self.working_kex.kex}"').replace("[tr]", f"[{self.working_kex.kex}]")

    async def run_query(self, query):
        return await self.connector.execute(query)

    async def clean_up(self):
        await super().clean_up()
        if self._connector:
            await self._connector.close()
