"""Class for executing transformation in Google BigQuery.
"""

import logging
from typing import Optional

from snowflake.connector import SnowflakeConnection

from toolkit.executors.transformation.base_sql import SqlTransformationExecutor
from toolkit.managers.component.sql import SQLTransformationComponentManager
from toolkit.managers.credentials import SnowflakeCredentialsManager
from toolkit.managers.storage import SnowflakeStorageManager
from toolkit.utils.stopwatch import stopwatch

logger = logging.getLogger(__name__)


class SnowflakeSqlTransformationExecutor(SqlTransformationExecutor):
    """Class for executing transformation in Snowflake."""

    DIALECT = "snowflake"

    def __init__(
        self,
        storage_manager: SnowflakeStorageManager,
        component_manager: SQLTransformationComponentManager,
        step,
        credentials_manager: SnowflakeCredentialsManager,
        inputs: list,
        output: str,
    ):
        super().__init__(storage_manager, component_manager, step, credentials_manager, inputs, output)
        self._snf_connection = None

    @stopwatch("Create environment", __qualname__)
    async def create_environment(self):
        await super().create_environment()
        # Need to refresh permission to OPERATOR
        await self.credentials_manager.grant_kex_permission_to_user(self.working_kex, "ORCHESTRATOR_ROLE")

    @property
    def snf_connection(self):
        if not self._snf_connection:
            credentials = self.credentials_manager.get_user_credentials(self.transformation_user)
            self._snf_connection = SnowflakeConnection(
                user=credentials["user"],
                password=credentials["password"],
                account=credentials["account"],
                warehouse=credentials["warehouse"],
                database=credentials["database"],
                role=f'"{self.transformation_user}"',
                schema=self.working_kex.kex,
                client_session_keep_alive=True,
            )
        return self._snf_connection

    def tr_replace(self, query):
        return query.replace('"tr"', '"{}"'.format(self.working_kex.kex))

    async def run_query(self, query):
        self.snf_connection.cursor().execute(query)
