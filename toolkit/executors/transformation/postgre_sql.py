"""Class for executing transformation in Azure SQL.
"""

import logging
from typing import Optional

from toolkit.executors.transformation.base_sql import SqlTransformationExecutor
from toolkit.managers.component.sql import SQLTransformationComponentManager
from toolkit.managers.credentials import PostgreSQLCredentialManager
from toolkit.managers.storage import PostgreSQLStorageManager
from toolkit.utils.postre_sql import PostgreSQLConnector
from toolkit.utils.stopwatch import stopwatch

logger = logging.getLogger(__name__)


class PostgreSQLTransformationExecutor(SqlTransformationExecutor):
    """Class for executing transformation in PostreSQL."""

    DIALECT = "postgres"

    def __init__(
        self,
        storage_manager: PostgreSQLStorageManager,
        component_manager: SQLTransformationComponentManager,
        step,
        credentials_manager: PostgreSQLCredentialManager,
        inputs: list,
        output: str,
    ):
        super().__init__(storage_manager, component_manager, step, credentials_manager, inputs, output)
        self._connector = None

    @property
    def connector(self):
        if not self._connector:
            credentials = self.credentials_manager.get_user_credentials(self.transformation_user)
            self._connector = PostgreSQLConnector(
                host=credentials["host"],
                database=credentials["database"],
                username=credentials["user"],
                password=credentials["password"],
                timeout=self.component_manager.query_timeout,
                port=credentials["port"],
            )
        return self._connector

    def tr_replace(self, query):
        return query.replace('"tr"', '"{}"'.format(self.working_kex.kex))

    async def run_query(self, query):
        return await self.connector.execute(query)

    async def clean_up(self):
        await super().clean_up()
        if self._connector:
            await self._connector.close()
