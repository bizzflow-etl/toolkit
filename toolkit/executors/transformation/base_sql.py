"""Class for SQL transformation execution.
Parent class for various SQL transformation executor classes, e.g. BqSqlTransformationExecutor.
"""

import logging
import uuid
from typing import Optional

from toolkit.base import Step
from toolkit.executors.base.sql import SQLExecutor, SQLInputOutputMixin
from toolkit.managers.component.sql import SQLTransformationComponentManager
from toolkit.managers.credentials.base import BaseCredentialsManager
from toolkit.managers.storage.base import BaseStorageManager
from toolkit.utils.stopwatch import stopwatch

logger = logging.getLogger(__name__)


class SqlTransformationExecutor(SQLInputOutputMixin, SQLExecutor):
    def __init__(
        self,
        storage_manager: BaseStorageManager,
        component_manager: SQLTransformationComponentManager,
        step: Step,
        credentials_manager: BaseCredentialsManager,
        inputs: list,
        output: str,
    ):
        super().__init__(storage_manager, component_manager, step, credentials_manager)
        self.transformation_user = self.working_kex.kex
        self.output = output
        self._inputs = inputs

    def _create_working_kex_name(self):
        return f"tr_{uuid.uuid4().hex}"

    def _create_output_kex_name(self):
        return self.output

    @stopwatch("Create environment", __qualname__)
    async def create_environment(self):
        """Create environment for transformation run.
        Update transformation account with permissions to temporary schema.
        Store transformation service account credentials in vault manager (if not stored already).
        """
        logger.info("Environment is being created")
        await super().create_environment()
        await self.credentials_manager.setup_user_for_kex(self.working_kex, self.transformation_user)

    @stopwatch("Clean environment", __qualname__)
    async def clean_environment(self):
        logger.info("Cleaning environment")
        await super().clean_environment()
        logger.info("Deleting temporary transformation user %s", self.transformation_user)
        await self.credentials_manager.delete_user(self.transformation_user)
        logger.info("Environment cleaned")

    @stopwatch("Run transformation", __qualname__)
    async def run(self, skip_files: Optional[list] = None, **kwargs):
        """Run transformation.
        Switch to transformation user for current transformation with default schema.

        Raises:
            ValueError: If a query fails.
        """
        for query in self.component_manager.get_queries(skip_files):
            query = self.tr_replace(query)
            with stopwatch(log_msg=query, caller_class=__class__.__name__):
                await self.run_query(query)

    def tr_replace(self, query):
        raise NotImplementedError

    async def run_query(self, query):
        raise NotImplementedError
