"""Datamart Executor

Module for managing all datamart executors on different platforms.
"""

import asyncio
import logging
from typing import List, Optional

from toolkit.base.kex import Kex
from toolkit.base.table import Table
from toolkit.executors.base.base import BaseExecutorWithKex
from toolkit.managers.credentials.base import BaseCredentialsManager
from toolkit.managers.storage.base import BaseStorageManager

logger = logging.getLogger(__name__)


class DatamartExecutor(BaseExecutorWithKex):
    """Class for all datamart executors on different platforms."""

    should_delete_working_kex = False

    def __init__(
        self,
        storage_manager: BaseStorageManager,
        credentials_manager: BaseCredentialsManager,
        out_kex: str,
        dm_kex: str,
        allowed_tables: Optional[List] = None,
    ):
        self.dm_kex_name = dm_kex
        self.out_kex = Kex.kex_from_str(out_kex)
        self.credentials_manager = credentials_manager
        self.allowed_tables = allowed_tables or []
        super().__init__(storage_manager)

    def _create_working_kex_name(self):
        return self.dm_kex_name

    async def run(self):
        """Run datamart writer."""
        await self.credentials_manager.setup_user_for_kex(self.working_kex, self.working_kex.kex, read_only=True)
        # copy tables from out to dm
        logger.info("Writing output tables to datamart %s", self.working_kex.get_id())
        out_tables = await self.storage_manager.list_tables(self.out_kex)
        tasks = []
        for table in out_tables:
            if (
                (not self.allowed_tables)
                or (table.table in self.allowed_tables)
                or (table.get_full_id() in self.allowed_tables)
            ):
                table_dm = Table(table.table, self.working_kex)
                tasks.append(self.storage_manager.copy_table(table, table_dm))
            else:
                logger.info("Skipping '%s' => not allowed", table.get_full_id())
        await asyncio.gather(*tasks)

    async def clean_up(self):
        await super().clean_up()
        await self.credentials_manager.close()
