"""Bizzflow Toolkit

See README.md
"""

from toolkit.managers.configuration.current_config import current_config  # noqa
from toolkit.version import __version__  # noqa
