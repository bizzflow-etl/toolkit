"""Various Airflow plugins

    - custom menu items
    - flow ui login
    - external dag sensor (to enable orchestration chaining)
"""

import datetime

from airflow import AirflowException
from airflow.configuration import conf
from airflow.models import DagBag
from airflow.plugins_manager import AirflowPlugin
from airflow.sensors.base import BaseSensorOperator
from airflow.sensors.external_task import ExternalDagLink
from airflow.utils import timezone
from airflow.utils.decorators import apply_defaults
from airflow.utils.session import provide_session
from airflow.utils.state import State
from airflow.www.auth import has_access

import jwt
from flask import g, make_response, redirect, request
from flask_appbuilder import BaseView, expose

from toolkit.version import __version__

bizzflow_category_name = f"Bizzflow {__version__}"

appbuilder_cooltivator = {
    "name": "Cooltivator",
    "category": bizzflow_category_name,
    "href": "https://cooltivator.appspot.com/",
}

bizzflow_docs = {"name": "Documentation", "category": bizzflow_category_name, "href": "https://wiki.bizzflow.net"}


class ExternalDAGSensor(BaseSensorOperator):
    """Waits for a different DAG to complete for a specific end_date interval"""

    template_fields = ["external_dag_id", "data_age"]
    operator_extra_links = [ExternalDagLink()]

    @apply_defaults
    def __init__(self, external_dag_id: str, data_age: datetime.timedelta = None, *args, **kwargs):
        super(ExternalDAGSensor, self).__init__(*args, **kwargs)

        if data_age is None or not isinstance(data_age, datetime.timedelta):
            raise ValueError("data_age must be a valid datetime.timedelta object")

        self.data_age = data_age
        self.external_dag_id = external_dag_id

    @provide_session
    def poke(self, context, session=None):
        """Check whether the dag is running"""

        now = timezone.utcnow()
        reference_date = context["ti"].start_date - self.data_age
        self.log.info(
            "Poking for DAG %s to have been run (end) between %s and %s", self.external_dag_id, reference_date, now
        )

        dagbag = DagBag(read_dags_from_db=True)

        target_dag = dagbag.get_dag(self.external_dag_id)

        if target_dag is None:
            raise ValueError("The specified DAG {} does not exist".format(self.external_dag_id))

        latest_run = target_dag.get_last_dagrun(include_externally_triggered=True)

        if latest_run is None:
            self.log.info("DAG %s has never run yet", self.external_dag_id)
            trigger_dag = True
        elif latest_run.end_date is None:
            duration = now - latest_run.start_date
            self.log.info(
                "DAG %s is still running, started at %s, duration %s",
                self.external_dag_id,
                latest_run.start_date,
                duration,
            )
            return False
        elif latest_run.end_date < reference_date:
            self.log.info("Latest DAG %s has not run in specified interval", self.external_dag_id)
            trigger_dag = True
        elif latest_run.state == State.FAILED:
            self.log.warning("Last run of required DAG %s failed", self.external_dag_id)
            if not latest_run.external_trigger:
                raise AirflowException("Last run of required DAG {} failed".format(self.external_dag_id))
            self.log.info("The failed run was a manual trigger, we will attempt to rerun.")
            trigger_dag = True
        elif latest_run.state == State.SUCCESS:
            duration = latest_run.end_date - latest_run.start_date
            self.log.info("Requirement fulfilled, DAG %s has been run, duration: %s", self.external_dag_id, duration)
            return True
        else:
            raise AirflowException("Unknown state for last run of required DAG {}".format(self.external_dag_id))

        if trigger_dag:
            self.log.info("Scheduling dagrun for dag %s", self.external_dag_id)
            target_dag.create_dagrun(State.NONE, run_id=self._get_run_id())
            return False

    def _get_run_id(self):
        """Return run id for current task"""
        return "required_{}".format(timezone.utcnow().isoformat())


class FlowUILogin(BaseView):
    """Generate jwt to be used with Flow UI"""

    route_base = "/flowui-login/"
    default_view = "login"
    base_permissions = ["can_access"]
    class_permission_name = "FlowUI"

    @expose("/", methods=["GET"])
    @has_access(
        [
            ("can_access", class_permission_name),
        ]
    )
    def login(self):
        """Generate jwt based on current user"""
        user_data = {
            "id": g.user.id,
            "name": f"{g.user.first_name} {g.user.last_name}",
            "email": g.user.email,
            "exp": datetime.datetime.utcnow() + datetime.timedelta(hours=1),
        }
        secret_key = conf.get("webserver", "SECRET_KEY")
        encoded_jwt = jwt.encode(user_data, secret_key, algorithm="HS256")
        url = request.args.get("next", "/flow/")
        response = make_response(redirect(url))
        response.set_cookie("flowui_session", encoded_jwt)
        return response


class BizzflowPlugin(AirflowPlugin):
    """Custom menu items"""

    name = "BizzflowPlugin"
    appbuilder_menu_items = [appbuilder_cooltivator, bizzflow_docs]
    sensors = [ExternalDAGSensor]
    appbuilder_views = [{"name": "Flow UI", "category": bizzflow_category_name, "view": FlowUILogin()}]
