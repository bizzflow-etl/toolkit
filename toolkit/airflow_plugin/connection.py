"""Airflow plugin to add Bizzflow Vault connection type"""

from airflow.hooks.base import BaseHook


class BizzflowVaultHook(BaseHook):
    """Bizzflow Vault connection type"""

    conn_name_attr = "conn_id"
    default_conn_name = "bizzflow_default"
    conn_type = "bizzflow_vault"
    hook_name = "Bizzflow Vault"

    @classmethod
    def get_ui_field_behaviour(cls):
        """Returns custom field behaviour"""
        return {
            "hidden_fields": ["host", "schema", "port", "login"],
            "relabeling": {
                "conn_id": "Secret name",
                "password": "Secret value",
            },
        }


def get_provider_info():
    """Return plugin metadata"""
    return {
        "package-name": "bizzflow-toolkit",
        "name": "Bizzflow Provider",
        "description": "Bizzflow Provider",
        "connection-types": [
            {
                "hook-class-name": "toolkit.airflow_plugin.connection.BizzflowVaultHook",
                "connection-type": "bizzflow_vault",
            },
        ],
    }
