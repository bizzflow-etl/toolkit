"""Transformations DAGs helpers"""

import logging

from toolkit import current_config
from toolkit.dags.helpers.base import PythonTaskCreator

logger = logging.getLogger(__name__)


class NotificationTaskCreator(PythonTaskCreator):
    """Create transformation tasks"""

    ui_color = "#FF00AE"
    task_type = "Notification"

    def __init__(self, notification_id: str, **kwargs) -> None:
        super().__init__(f"nt_{notification_id}", **kwargs)
        self.notification_id = notification_id

    async def python_callable(self, **kwargs):
        """Execute transformation"""
        notification_executor = current_config.get_notification_executor(self.notification_id)
        await notification_executor.execute()
