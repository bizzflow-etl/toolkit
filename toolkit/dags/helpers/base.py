"""Bases for dag helpers - task creators using Airflow operators"""

import asyncio
import logging
from abc import ABC
from datetime import datetime, timedelta

from airflow import DAG, AirflowException
from airflow.models import BaseOperator
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow.utils.trigger_rule import TriggerRule

from toolkit import current_config
from toolkit.dags.helpers.pools import PoolManager
from toolkit.dags.helpers.utils import notify_email
from toolkit.utils.stopwatch import stopwatch

logger = logging.getLogger(__name__)

DEFAULT_TIMEOUT = 36000


class BaseTaskCreator(ABC):
    operator_class = BaseOperator
    ui_color: str = "#fff"
    task_type = "BaseTask"

    def __init__(self, task_id: str, **kwargs) -> None:
        self.task_id = task_id
        self.params = {}
        notify = kwargs.get("notify", None)
        if notify is not None:
            self.params["notify"] = notify
        self.params["continue_on_error"] = kwargs.get("continue_on_error", False)
        self.requires_worker = kwargs.get("requires_worker", False)
        self.trigger_rule = kwargs.get("trigger_rule", None)
        self.kwargs = kwargs
        self.on_failure_callback = kwargs.get("on_failure_callback", notify_email)
        PoolManager().create_pool_if_not_exists(task_id)

    def get_execution_timeout(self):
        timeout = self.kwargs.get("timeout", DEFAULT_TIMEOUT) or DEFAULT_TIMEOUT
        try:
            timeout = int(timeout)
        except ValueError as error:
            raise ValueError(
                f"'timeout' parameter must be a valid integer, got '{timeout}' for task '{self.task_id}'"
            ) from error
        if timeout == -1:
            # no timeout
            execution_timeout = None
        elif timeout < 0:
            logger.warning(
                "Invalid timeout parameter value %d (must be -1, unset or positive) for task '%s'. Using default %d s",
                timeout,
                self.task_id,
                DEFAULT_TIMEOUT,
            )
            execution_timeout = timedelta(seconds=DEFAULT_TIMEOUT)
        else:
            execution_timeout = timedelta(seconds=timeout)
            logger.info("Task %s's timeout is set to %d s", self.task_id, timeout)
        return execution_timeout

    def get_operator_class(self):
        return type(f"Bizzflow{self.task_type}", (self.operator_class,), {"ui_color": self.ui_color})

    def get_operator_kwargs(self):
        kwargs = {
            "on_failure_callback": self.on_failure_callback,
            "pool": self.task_id,
            "params": self.params,
            "execution_timeout": self.get_execution_timeout(),
        }
        if self.trigger_rule:
            kwargs.update({"trigger_rule": self.trigger_rule})
        return kwargs

    def create(self, dag) -> get_operator_class:
        Operator = self.get_operator_class()
        return Operator(task_id=self.task_id, dag=dag, **self.get_operator_kwargs())


class BashTaskCreator(BaseTaskCreator):
    operator_class = BashOperator

    def __init__(self, task_id: str, **kwargs) -> None:
        super().__init__(task_id, **kwargs)

    def get_operator_kwargs(self):
        kwargs = super().get_operator_kwargs()
        kwargs["bash_command"] = self.bash_command
        return kwargs

    @property
    def bash_command(self) -> str:
        raise NotImplementedError


class PythonTaskCreator(BaseTaskCreator):
    operator_class = PythonOperator
    task_type = "PythonOperator"

    def __init__(self, task_id: str, **kwargs) -> None:
        super().__init__(task_id, **kwargs)

    def get_operator_kwargs(self):
        kwargs = super().get_operator_kwargs()
        kwargs["python_callable"] = self._python_callable_wrapper
        return kwargs

    def _python_callable_wrapper(self, **kwargs):
        asyncio.run(self.python_callable(**kwargs))

    async def python_callable(self, **kwargs):
        raise NotImplementedError


class StopWorkerTaskCreator(PythonTaskCreator):
    task_type = "StopWorker"
    ui_color = "#AEAEAE"

    def __init__(self, **kwargs) -> None:
        super().__init__(
            "stop_worker", continue_on_error=True, trigger_rule=TriggerRule.ALL_DONE, notify=True, **kwargs
        )

    async def python_callable(self, **kwargs):
        logger.info("Shutting down worker machine")
        with stopwatch("Shut down worker machine attempt", __class__.__name__):
            worker_manager = current_config.loader.get_worker_manager()
            if worker_manager.keep_running:
                logger.info("Not stopping worker, keep_running is True")
                return
            if worker_manager.get_running():
                result = worker_manager.run("[ ! -s RUNNING_COMPONENTS ]", warn=True)
                if result.ok:
                    logger.info("Shutting down worker machine")
                    worker_manager.stop()
                else:
                    logger.info("At least one docker component is running on worker, do not shut it down")
            else:
                logger.info("Not stopping, no running worker detected")


class IgnoreLeafTaskCreator(PythonTaskCreator):
    task_type = "HelperTask"
    ui_color = "#AEAEAE"

    def __init__(self, **kwargs) -> None:
        kwargs.pop("notify", None)
        super().__init__(
            "ignore_leaf",
            continue_on_error=True,
            trigger_rule=TriggerRule.ONE_FAILED,
            notify=False,
            **kwargs,
        )

    async def python_callable(self, **kwargs):
        logger.info("When triggered, this task should always succeed and prevent leaf from failing")


class DagStatusTaskCreator(PythonTaskCreator):
    task_type = "HelperTask"
    ui_color = "#AEAEAE"

    def __init__(self, **kwargs) -> None:
        kwargs.pop("notify", None)
        super().__init__(
            "dag_status",
            trigger_rule=TriggerRule.ONE_FAILED,
            retries=0,
            notify=False,
            **kwargs,
        )

    async def python_callable(self, **kwargs):
        raise AirflowException("Failing task because one or more upstream tasks failed.")


class TaskGenerator(ABC):
    def generate(self, dag: DAG):
        raise NotImplementedError


class SingleTaskGenerator(TaskGenerator):
    def __init__(self, task_creator: BaseTaskCreator):
        self.task_creator = task_creator

    def generate(self, dag: DAG) -> None:
        main_task = self.task_creator.create(dag)
        if not current_config.loader.get_worker_manager().keep_running and self.task_creator.requires_worker:
            stop_worker_task = StopWorkerTaskCreator().create(dag)
            ignore_leaf_task = IgnoreLeafTaskCreator().create(dag)
            dag_status_task = DagStatusTaskCreator().create(dag)
            main_task >> stop_worker_task >> ignore_leaf_task
            main_task >> dag_status_task


class DagCreator:
    DEFAULT_ARGS = {
        "owner": "bizzflow",
        "depends_on_past": False,
        "start_date": datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=31),
        "email_on_failure": False,
        "email": current_config.notification_emails[0] if current_config.notification_emails else None,
        "retries": 0,
        "retry_delay": timedelta(seconds=30),
        "catchup": False,
        "schedule": None,
    }
    DEFAULT_DAG_KWARGS = {
        "schedule": None,
        "catchup": False,
        "start_date": datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=31),
    }

    def __init__(self, dag_id: str, task_generator: TaskGenerator, **kwargs) -> None:
        self.dag_id = dag_id
        self.task_generator = task_generator
        self.default_args = {**self.DEFAULT_ARGS, **kwargs.pop("default_args", {})}
        self.dag_kwargs = {**self.DEFAULT_DAG_KWARGS, **kwargs}

    def create(self):
        dag = DAG(dag_id=self.dag_id, default_args=self.default_args, **self.dag_kwargs)
        self.task_generator.generate(dag)
        return dag
