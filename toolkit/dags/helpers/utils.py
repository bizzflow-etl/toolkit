"""DAG generator utils"""

import logging
import os
from typing import Optional

from airflow.datasets import Dataset
from airflow.utils.email import send_email
from airflow.utils.log.log_reader import TaskLogReader

from toolkit import current_config
from toolkit.managers.configuration.orchestration import Orchestration

logger = logging.getLogger(__name__)


def get_notify_param(context: dict) -> bool:
    """Check whether we are supposed to send notification or not

    Arguments:

        context {dict} -- Airflow task context variable, but any dictionary will suffice
                            as long as it contains `dag_run`: `DagRun` and `params`: `dict` keys
    """
    external_notify: Optional[bool] = (context.get("dag_run").conf or {}).get("notify")
    # external override (using context configuration JSON in Trigger DAG function)
    if external_notify is not None:
        return external_notify
    # basic settings
    # this is set via DAG and both via Task, task setting suppresses DAG settings,
    # and so we can still have cases when only single task raises notification
    # or only a single task lets it fly
    task_notify: Optional[bool] = context.get("params", {}).get("notify")
    if task_notify is not None:
        return task_notify
    # if we missed anything, better send the notification
    return True


def get_notification_level(context: dict) -> str:
    """Guess whether current failure should raise warning or error notification

    Arguments:

        context {dict} -- Airflow task context variable, but any dictionary will suffice
                            as long as it contains `dag_run`: `DagRun` and `params`: `dict` keys
    """
    continue_on_error: bool = context.get("params", {}).get("continue_on_error", False)
    if continue_on_error:
        return "warning"
    return "error"


def notify_email(contextDict, **kwargs):
    """Send custom email alerts from airflow."""

    notification_emails = current_config.notification_emails
    notify = get_notify_param(contextDict)
    if not notify:
        logger.info("Not sending notification due to configuration")
        return
    if not notification_emails:
        logger.warning("Notification email is not specified, no notification will be sent.")
        return
    logger.info("Constructing notification email")
    template_location = os.path.join(os.path.dirname(__file__), "template.html")
    with open(template_location, encoding="utf-8") as fid:
        template = fid.read()
    ti = contextDict["task_instance"]
    try:
        log_reader = TaskLogReader()
        logs, metadata = log_reader.read_log_chunks(ti, ti.prev_attempted_tries, {})
        logs = logs[0][0][1]
        log_tail = "\n".join([line for line in logs.split("\n")][-50:])
    # Not sure what exceptions can be raised here, so catch all, we don't want to fail the task
    except Exception:  # noqa
        log_tail = "Failed to fetch logs"

    notification_level = get_notification_level(contextDict)
    # email title.
    title = "Bizzflow: {dag._dag_id} - {task.task_id} failed".format(**contextDict)

    # email contents
    body = template.format(**contextDict, log=log_tail, level=notification_level)
    for email in notification_emails:
        logger.info("Sending notification to %s", ", ".join(notification_emails))
        send_email(email, title, body, mime_charset="utf-8")


def get_dataset(dataset_name: str):
    """Get datasets from configuration"""
    return Dataset(f"bizzflow://{dataset_name}")


def get_orchestration_schedule(orchestration: Orchestration):
    """Get orchestration schedule"""
    if orchestration.cron_expression:
        return orchestration.cron_expression
    if orchestration.datasets:
        return [get_dataset(dataset_name) for dataset_name in orchestration.datasets]
    return None
