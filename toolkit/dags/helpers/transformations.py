"""Transformations DAGs helpers"""

import logging

from toolkit import current_config
from toolkit.dags.helpers.base import PythonTaskCreator

logger = logging.getLogger(__name__)


class TransformationTaskCreator(PythonTaskCreator):
    """Create transformation tasks"""

    @property
    def ui_color(self):
        """UI color based on transformation type"""
        if self.requires_worker:
            return "#6EB9FF"
        return "#6EEBFF"

    @property
    def task_type(self):
        """Task type based on transformation type"""
        if self.requires_worker:
            return "DockerTransformation"
        return "Transformation"

    def __init__(self, transformation_id: str, **kwargs) -> None:
        requires_worker = self.get_requires_worker(transformation_id)
        super().__init__(f"tr_{transformation_id}", requires_worker=requires_worker, **kwargs)
        self.transformation_id = transformation_id

    async def python_callable(self, **kwargs):
        """Execute transformation"""
        transformation_executor = current_config.get_transformation_executor(self.transformation_id)
        await transformation_executor.execute()

    @staticmethod
    def get_requires_worker(transformation_id):
        """Get whether current transformation requires worker"""
        transformation_type = current_config.get_transformation_type(transformation_id)
        if transformation_type == "docker":
            return True
        else:
            return False
