"""Dummy task to test orchestrations easily
"""

import logging

from airflow.datasets import Dataset

from toolkit.dags.helpers.base import PythonTaskCreator
from toolkit.dags.helpers.utils import get_dataset

logger = logging.getLogger(__name__)


class DatasetRefreshTaskCreator(PythonTaskCreator):
    ui_color = "#78A9FF"
    task_type = "DatasetRefresh"

    def __init__(self, dataset_id: str, **kwargs) -> None:
        super().__init__(f"dataset_refresh_{dataset_id}", **kwargs)
        self.dataset_id = dataset_id

    async def python_callable(self, **kwargs):
        logger.info("Set dataset as refreshed")

    def get_operator_kwargs(self):
        kwargs = super().get_operator_kwargs()
        kwargs["outlets"] = [get_dataset(self.dataset_id)]
        return kwargs
