"""Extractor DAGs helper"""

import logging

from toolkit import current_config
from toolkit.dags.helpers.base import PythonTaskCreator

logger = logging.getLogger(__name__)


class ExtractorTaskCreator(PythonTaskCreator):
    """Create extractor tasks"""

    ui_color = "#FFC35B"
    task_type = "Extractor"
    # operator_extra_links = [ExternalDagLink()]

    def __init__(self, extractor_id: str, **kwargs) -> None:
        super().__init__(f"ex_{extractor_id}", requires_worker=True, **kwargs)
        self.extractor_id = extractor_id

    async def python_callable(self, **kwargs):
        """Execute extraction"""
        extractor_executor = current_config.get_extractor_executor(self.extractor_id)
        await extractor_executor.execute()
