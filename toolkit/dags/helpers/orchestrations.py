"""Helpers for orchestration DAG creators"""

import logging
from datetime import timedelta
from typing import List

from airflow import DAG
from airflow.operators.empty import EmptyOperator
from airflow.utils.trigger_rule import TriggerRule

from toolkit import current_config
from toolkit.airflow_plugin.plugin import ExternalDAGSensor
from toolkit.dags.helpers.base import (
    BaseTaskCreator,
    DagStatusTaskCreator,
    IgnoreLeafTaskCreator,
    StopWorkerTaskCreator,
    TaskGenerator,
)
from toolkit.dags.helpers.datamarts import DatamartTaskCreator
from toolkit.dags.helpers.dataset import DatasetRefreshTaskCreator
from toolkit.dags.helpers.dummies import DummyTaskCreator
from toolkit.dags.helpers.extractors import ExtractorTaskCreator
from toolkit.dags.helpers.notifications import NotificationTaskCreator
from toolkit.dags.helpers.transformations import TransformationTaskCreator
from toolkit.dags.helpers.writers import WriterTaskCreator
from toolkit.managers.configuration.orchestration import BaseTask

logger = logging.getLogger(__name__)


class SubDagTaskCreator(BaseTaskCreator):
    """Task creator for running any other dag and waiting for it to succeed."""

    ui_color = "#19647e"
    task_type = "SubDag"
    operator_class = ExternalDAGSensor

    def __init__(
        self,
        dag_id: str,
        data_age=None,
        timeout=None,
        poke_interval=None,
        dependency_mode=None,
        **kwargs,
    ):
        task_suffix = dag_id.replace("00_Orchestration_", "")  # Use backwards-compatible task id
        task_id = f"required_{task_suffix}"
        self.external_dag_id = dag_id
        super().__init__(task_id, **kwargs)
        self.timeout = timeout
        if self.timeout is None:
            logger.warning("-> Sub-dag %s has no timeout set, default is 6 hours", dag_id)
            self.timeout = 6 * 3600
        self.poke_interval = poke_interval
        if self.poke_interval is None:
            logger.warning("-> Sub-dag %s has no poke interval set, default is 10 minutes", dag_id)
            self.poke_interval = 10 * 60
        self.dependency_mode = dependency_mode
        if self.dependency_mode is None:
            logger.warning("-> Sub-dag %s has no dependency mode, default is reschedule", dag_id)
            self.dependency_mode = "reschedule"
        self.data_age = data_age

    def get_operator_kwargs(self):
        kwargs = super().get_operator_kwargs()
        kwargs["external_dag_id"] = self.external_dag_id
        kwargs["data_age"] = timedelta(seconds=self.data_age)
        kwargs["poke_interval"] = self.poke_interval
        kwargs["mode"] = self.dependency_mode
        kwargs["timeout"] = self.timeout
        return kwargs


class TaskCreatorFactory:
    """Get task creator based on task type"""

    TASK_DICT = {
        "extractor": ExtractorTaskCreator,
        "transformation": TransformationTaskCreator,
        "datamart": DatamartTaskCreator,
        "writer": WriterTaskCreator,
        "subdag": SubDagTaskCreator,
        "dummy": DummyTaskCreator,
        "notification": NotificationTaskCreator,
        "dataset_refresh": DatasetRefreshTaskCreator,
    }

    @classmethod
    def get_task_creator(cls, task: BaseTask):
        """Get task creator based on task"""
        try:
            return cls.TASK_DICT[task.type](task.id, **task.config)
        except KeyError as error:
            logger.error("Executor of type %s is not implemented", task.type)
            raise NotImplementedError(f"Executor of type {task.type} is not implemented") from error


class OrchestrationTaskGenerator(TaskGenerator):
    """Generator used to create flow of tasks within orchestration"""

    def __init__(self, tasks: List[List[BaseTask]]) -> None:
        super().__init__()
        self.tasks = tasks
        self._stop_worker_task = None
        self._ignore_leaf_task = None
        self._dag_status_task = None

    def get_stop_worker_task(self, dag):
        """Generate stop_worker task"""
        if not self._stop_worker_task:
            self._stop_worker_task = StopWorkerTaskCreator().create(dag)
        return self._stop_worker_task

    def get_ignore_leaf_task(self, dag):
        """Generate ignore_leaf task"""
        if not self._ignore_leaf_task:
            self._ignore_leaf_task = IgnoreLeafTaskCreator().create(dag)
        return self._ignore_leaf_task

    def get_dag_status_task(self, dag):
        """Generate dag_status task"""
        if not self._dag_status_task:
            self._dag_status_task = DagStatusTaskCreator().create(dag)
        return self._dag_status_task

    def generate(self, dag: DAG):
        """Loop through tasks and task groups and create tasks for specified dag.
        The dag will contain all tasks the orchestration is supposed to have.
        """
        previous_tasks = []
        last_mandatory_tasks = []
        stop_worker_connector_candidates = []
        set_status_connector_candidates = []
        for task_group in self.tasks:
            current_tasks = []
            current_mandatory_tasks = []
            current_stop_worker_connector_candidates = []
            current_set_status_connector_candidates = []
            for task in task_group:
                current_task_creator = TaskCreatorFactory.get_task_creator(task)
                current_task = current_task_creator.create(dag)

                for previous_task in previous_tasks:
                    previous_task >> current_task  # pylint: disable=pointless-statement
                for last_mandatory_task in last_mandatory_tasks:
                    last_mandatory_task >> current_task  # pylint: disable=pointless-statement

                if current_task_creator.requires_worker:
                    current_stop_worker_connector_candidates.append(current_task)
                    if not task.continue_on_error:
                        current_set_status_connector_candidates.append(current_task)
                if task.continue_on_error:
                    # if continue on error - make a group of tasks - add a dummy always success
                    # task after the real one this will cause that this group of tasks will be
                    # always successful independently on the task itself
                    task_id = f"{current_task.task_id}_continue_on_error"
                    dummy_task = EmptyOperator(task_id=task_id, dag=dag, trigger_rule=TriggerRule.ALL_DONE)
                    current_task >> dummy_task  # pylint: disable=pointless-statement
                    current_task = dummy_task
                else:
                    current_mandatory_tasks.append(current_task)
                current_tasks.append(current_task)
            previous_tasks = current_tasks
            last_mandatory_tasks = current_mandatory_tasks or last_mandatory_tasks
            stop_worker_connector_candidates = (
                current_stop_worker_connector_candidates or stop_worker_connector_candidates
            )
            set_status_connector_candidates = current_set_status_connector_candidates
        if not current_config.loader.get_worker_manager().keep_running:
            for candidate in stop_worker_connector_candidates:
                candidate >> self.get_stop_worker_task(dag) >> self.get_ignore_leaf_task(dag)
            for candidate in set_status_connector_candidates:
                candidate >> self.get_dag_status_task(dag)
