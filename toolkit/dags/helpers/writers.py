"""Writers DAGs helper"""

import logging

from toolkit import current_config
from toolkit.dags.helpers.base import PythonTaskCreator

logger = logging.getLogger(__name__)


class WriterTaskCreator(PythonTaskCreator):
    """Create writer tasks"""

    ui_color = "#FF6EF2"
    task_type = "Writer"

    def __init__(self, writer_id: str, **kwargs) -> None:
        super().__init__(f"wr_{writer_id}", requires_worker=True, **kwargs)
        self.writer_id = writer_id

    async def python_callable(self, **kwargs):
        """Execute writer"""
        writer_executor = current_config.get_writer_executor(self.writer_id)
        await writer_executor.execute()
