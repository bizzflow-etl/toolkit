"""DAG helpers for refresh_sharing dag"""

import asyncio
import logging

from toolkit import current_config
from toolkit.dags.helpers.base import PythonTaskCreator

logger = logging.getLogger(__name__)


class RefreshSharingTaskCreator(PythonTaskCreator):
    """Create task for refresh_sharing dag"""

    def __init__(self):
        super().__init__("refresh_sharing", notify=False)

    async def python_callable(self, **kwargs):
        """Re-share tables specified in sharing configuration"""
        credentials_manager = current_config.get_credentials_manager()
        storage_manager = current_config.get_storage_manager()

        for table in storage_manager.list_shared_out_tables():
            for destination_project in storage_manager.shared_out_table_destinations(table):
                logger.info("Sharing table %s to project %s", table.table, destination_project)
                user_name = f"sh_{destination_project}"
                await credentials_manager.create_sharing_user(user_name)
                await credentials_manager.share_table_to_user(user_name, table)
        await storage_manager.close()
        await credentials_manager.close()
