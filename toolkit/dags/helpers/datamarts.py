"""Datamarts DAG helper"""

import logging

from toolkit import current_config
from toolkit.dags.helpers.base import PythonTaskCreator

logger = logging.getLogger(__name__)


class DatamartTaskCreator(PythonTaskCreator):
    """Create datamart tasks"""

    ui_color = "#BD6EFF"
    task_type = "Datamart"

    def __init__(self, datamart_id: str, **kwargs) -> None:
        super().__init__(f"dm_{datamart_id}", **kwargs)
        self.datamart_id = datamart_id

    async def python_callable(self, **kwargs):
        """Create datamart and its credentials"""
        datamart_executor = current_config.get_datamart_executor(self.datamart_id)
        await datamart_executor.execute()
