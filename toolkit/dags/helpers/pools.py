"""Pool creator to make sure there are no conflicts in concurrent tasks"""

import logging

from airflow.models import Pool
from airflow.settings import Session

from toolkit import current_config, metaclasses

logger = logging.getLogger(__name__)


class PoolManager(metaclass=metaclasses.Singleton):
    """Access Airflow's pools"""

    def __init__(self):
        self.session = None
        self.available_pools = []

    def update_airflow_pools(self):
        for extractor_id in current_config.get_extractors_ids():
            self.create_pool_if_not_exists(f"ex_{extractor_id}")
        for transformation_id in current_config.get_transformations_ids():
            self.create_pool_if_not_exists(f"tr_{transformation_id}")
        for datamart_id in current_config.get_datamarts_ids():
            self.create_pool_if_not_exists(f"dm_{datamart_id}")
        for writer_id in current_config.get_writers_ids():
            self.create_pool_if_not_exists(f"wr_{writer_id}")

    def create_pool_if_not_exists(self, pool_name: str):
        """Create pool if it does not exist yet"""
        if self.session is None:
            self.open_session()
        if pool_name not in self.available_pools:
            logger.info(f"Creating worker pool {pool_name}")
            worker_pool = Pool(
                pool=pool_name,
                slots=1,
                description="Making sure only one task with same id will run just once at the same time",
                include_deferred=False,
            )
            self.session.add(worker_pool)
            self.session.commit()
            self.available_pools.append(pool_name)

    def open_session(self):
        """Open a new Airflow session"""
        self.close_session()
        self.session = Session()
        self.available_pools = [p.pool for p in self.session.query(Pool).all()]

    def close_session(self):
        """Close existing Airflow session"""
        if self.session:
            self.session.close()
        self.session = None

    def __del__(self):
        """Close session on deletion"""
        self.close_session()
