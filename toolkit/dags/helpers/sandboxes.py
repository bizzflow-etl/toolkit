"""Sandbox DAG helper"""

import logging

from toolkit import current_config
from toolkit.dags.helpers.base import PythonTaskCreator

logger = logging.getLogger(__name__)


class SandboxTaskCreator(PythonTaskCreator):
    """Create sandbox tasks"""

    def __init__(self):
        super().__init__(
            "sandbox",
            notify=False,
        )

    def get_operator_kwargs(self):
        """Get airflow operator kwargs"""
        kwargs = super().get_operator_kwargs()
        kwargs["provide_context"] = True
        return kwargs

    async def python_callable(self, **kwargs):
        """Create sandbox and execute transformation (if configured)"""
        dag_run_config = kwargs["dag_run"].conf
        transformation_id = dag_run_config.get("transformation_id")
        sandbox_user_email = dag_run_config["sandbox_user_email"]
        clean_sandbox = dag_run_config.get("clean_sandbox", True)
        load_transformation = dag_run_config.get("load_transformation", True)
        additional_kexes = dag_run_config.get("additional_kexes")
        additional_tables = dag_run_config.get("additional_tables")
        dry_run = dag_run_config.get("dry_run", False)
        run_options = dag_run_config.get("run_options", {})

        if transformation_id is None:
            # transformation executor without properly defined component config (input mapping and queries) can just create sandbox
            load_transformation = False
            dry_run = False

        sandbox_manager = current_config.get_sandbox_manager(sandbox_user_email, transformation_id)

        await sandbox_manager.create_sandbox()
        if clean_sandbox:
            await sandbox_manager.clean_sandbox()
        await sandbox_manager.load(
            load_transformation=load_transformation,
            additional_kexes=additional_kexes,
            additional_tables=additional_tables,
        )
        if dry_run:
            await sandbox_manager.dry_run(**run_options)
