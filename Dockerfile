FROM python:3.11-slim
SHELL ["/bin/bash", "-o", "pipefail", "-e", "-u", "-x", "-c"]

ARG VERSION="unknown"
ENV VERSION=${VERSION}
ARG GIT_SHA="unknown"
ENV GIT_SHA=${GIT_SHA}

LABEL app.bizzflow.name="Bizzflow" \
    app.bizzflow.author="Bizztreat <info@bizztreat.com>" \
    app.bizzflow.version="${VERSION}" \
    app.bizzflow.git_sha="${GIT_SHA}" \
    app.bizzflow.port="8080"


RUN apt-get update -y && apt-get install -y build-essential curl gcc libpq-dev python3-dev git unzip apt-transport-https ca-certificates gnupg

# Install gcloud as it is still used within GCP Worker Manager
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | gpg --dearmor -o /usr/share/keyrings/cloud.google.gpg && \
    echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    apt-get update -y && apt-get install google-cloud-cli -y


# Install az client - still used in Azure Worker Manager
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

# Install aws cli - still used in AWS Worker Manager
RUN apt-get -y install unzip && \
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install

RUN  curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor -o /usr/share/keyrings/microsoft-prod.gpg && \
    curl https://packages.microsoft.com/config/debian/12/prod.list > /etc/apt/sources.list.d/mssql-release.list && \
    apt-get update && \
    ACCEPT_EULA=Y apt-get install -y msodbcsql18 && \
    apt-get install -y unixodbc-dev

# Install PostgreSQL ODBC driver and fix path in odbcinst.ini file
RUN apt-get -y install odbc-postgresql && \
    sed -i  's/Driver=psqlodbcw\.so/Driver=\/usr\/lib\/x86_64-linux-gnu\/odbc\/psqlodbcw\.so/' /etc/odbcinst.ini

# Copy docker bin (for docker in docker for local worker)
COPY --from=docker:dind /usr/local/bin/docker /usr/local/bin/

# Install poetry
RUN curl -sSL https://install.python-poetry.org | python -
ENV PATH="$PATH:/root/.local/bin/"

WORKDIR /build

ADD poetry.lock /build
ADD pyproject.toml /build

RUN poetry config virtualenvs.create false && \
    poetry install --all-extras --no-root

RUN rm -rf /build

COPY . /toolkit
RUN cd /toolkit && poetry install --all-extras


RUN useradd -m -s /bin/bash bizzflow

USER bizzflow

WORKDIR /home/bizzflow/

RUN mkdir $HOME/projects && mkdir $HOME/airflow mkdir $HOME/airflow-plugins

# Airflow must be initialized with connection to the DB, so this will be done in the entrypoint section

RUN mkdir -p $HOME/dags && \
    ln -s /toolkit/toolkit/dags/dag_project.py $HOME/dags/dag_project.py && \
    ln -s /toolkit/toolkit/dags/dag_project_update.py $HOME/dags/dag_project_update.py && \
    ln -s /toolkit/toolkit/dags/dag_sandbox.py $HOME/dags/dag_sandbox.py && \
    ln -s /toolkit/toolkit/dags/dag_sharing.py $HOME/dags/dag_sharing.py && \
    ln -s /toolkit/toolkit/dags/dag_project_custom_dags.py $HOME/dags/dag_project_custom_dags.py && \
    ln -s /toolkit/toolkit/dags/dag_telemetry.py $HOME/dags/dag_telemetry.py  && \
    ln -s /toolkit/toolkit/airflow_plugin/bizzflow $HOME/airflow-plugins/bizzflow && \
    ln -s /toolkit/airflow_init.sh $HOME/airflow_init.sh && \
    ln -s /toolkit/airflow_config.py $HOME/airflow_config.py && \
    ln -s /toolkit/bizzflow_init.sh $HOME/bizzflow_init.sh && \
    ln -s /toolkit/bizzflow_config.py $HOME/bizzflow_config.py && \
    ln -s /toolkit/airflow_scheduler.sh $HOME/airflow_scheduler.sh && \
    ln -s /toolkit/airflow_webserver.sh $HOME/airflow_webserver.sh


RUN mkdir -p $HOME/.ssh && \
    echo -e "Host *\n    StrictHostKeyChecking no\n" | tee "$HOME/.ssh/config"

EXPOSE 8080
