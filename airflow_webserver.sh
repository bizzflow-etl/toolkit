#!/bin/bash

# distill ssh key from env variable
if [ ! -f "~/.ssh/id_rsa" ]; then
    mkdir -p ~/.ssh/
    echo -e "${GIT_SSH_KEY}" | tee ~/.ssh/id_rsa > /dev/null
    chmod 400 ~/.ssh/id_rsa
fi

# AWS cli login
if [ "$PLATFORM" == "aws" ]; then
    aws configure --profile default set output json
    aws configure --profile default set region "${COMPUTE_REGION}"
fi

# Azure cli login
if [ "$PLATFORM" == "azure" ]; then
    az login --identity
fi

airflow webserver
