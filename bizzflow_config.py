import json
import os
import shutil

project_config_file_path = os.path.join(os.getenv("BIZZFLOW_PROJECT_PATH", "."), "project.json")
transformations_folder_path = os.path.join(os.getenv("BIZZFLOW_PROJECT_PATH", "."), "transformations")
transformation_prefix = None

with open(project_config_file_path) as project_config_file:
    project_config = json.load(project_config_file)

project_config["project_id"] = os.getenv("GCP_PROJECT_ID", "")
project_config["git_project_path"] = os.getenv("BIZZFLOW_PROJECT_GIT", "")
project_config["live_bucket"] = os.getenv("BUCKET_LIVE", "")
project_config["archive_bucket"] = os.getenv("BUCKET_ARCHIVE", "")

project_config["compute_zone"] = os.getenv("COMPUTE_ZONE", "")
project_config["compute_region"] = os.getenv("COMPUTE_REGION", "")

project_config["worker_machine"] = [
    {
        "id": os.getenv("WORKER_ID", ""),
        "name": os.getenv("WORKER_NAME", ""),
        "host": os.getenv("WORKER_IP_ADDRESS", ""),
        "user": "bizzflow",
        "components_path": "/home/bizzflow/components",
        "data_path": "/home/bizzflow/data",
        "config_path": "/home/bizzflow/config",
    }
]
project_config["hostname"] = os.getenv("BIZZFLOW_HOSTNAME", "")
project_config["public_ip"] = os.getenv("AIRFLOW_PUBLIC_IP_ADDRESS", "")
project_config["notification_email"] = [os.getenv("WEB_USER_EMAIL", "")]


PLATFORM = os.getenv("PLATFORM")
if PLATFORM is None:
    raise ValueError("PLATFORM env variable was not set")

if PLATFORM == "gcp":
    project_config["dataset_location"] = os.getenv("DATASET_LOCATION", "EU")

    project_config["classes"] = {
        "storage_manager": "BqStorageManager",
        "sandbox_manager": "BqSqlSandboxManager",
        "vault_manager": "AirflowVaultManager",
        "worker_manager": "GcpWorkerManager",
        "file_storage_manager": "GcsFileStorageManager",
        "datamart_manager": "BqDatamartManager",
        "credentials_manager": "GcpCredentialsManager",
        "transformation_executor": "BqSqlTransformationExecutor",
        "step": "BQStep",
    }
    transformation_prefix = "bigquery."

elif PLATFORM == "aws":
    project_config["classes"] = {
        "storage_manager": "SnowflakeStorageManager",
        "sandbox_manager": "SnowflakeSqlSandboxManager",
        "vault_manager": "AirflowVaultManager",
        "worker_manager": "AwsWorkerManager",
        "file_storage_manager": "S3FileStorageManager",
        "datamart_manager": "SnowflakeDatamartManager",
        "credentials_manager": "SnowflakeCredentialsManager",
        "transformation_executor": "SnowflakeSqlTransformationExecutor",
        "step": "SnowflakeStep",
    }
    project_config["storage"] = {
        "backend": "snowflake",
        "account": os.getenv("SNOWFLAKE_ACCOUNT", ""),
        "database": os.getenv("SNOWFLAKE_DATABASE", ""),
        "warehouse": os.getenv("SNOWFLAKE_WAREHOUSE", ""),
    }
    transformation_prefix = "snowflake."

elif PLATFORM == "azure":
    project_config["azure_blob_account_name"] = os.getenv("AZURE_STORAGE_ACCOUNT_NAME", "")
    project_config["resource_group"] = os.getenv("AZURE_RESOURCE_GROUP", "")
    project_config["classes"] = {
        "storage_manager": "AzureSQLStorageManager",
        "sandbox_manager": "AzureSqlSandboxManager",
        "vault_manager": "AirflowVaultManager",
        "worker_manager": "AzureWorkerManager",
        "file_storage_manager": "ABSFileStorageManager",
        "datamart_manager": "AzureSQLDatamartManager",
        "credentials_manager": "AzureSQLCredentialManager",
        "transformation_executor": "AzureSQLTransformationExecutor",
        "step": "AzureSQLStep",
    }
    project_config["storage"] = {
        "host": os.getenv("SQL_SERVER_HOST", ""),
        "database": os.getenv("SQL_SERVER_DB", ""),
        "port": int(os.getenv("SQL_SERVER_PORT", 1433)),
        "backend": "azuresql",
    }
    transformation_prefix = "azuresql."

elif PLATFORM == "onprem":
    project_config["telemetry"] = {"generate": True}
    project_config["live_bucket"] = "/home/bizzflow/file-storage/live"
    project_config["archive_bucket"] = "/home/bizzflow/file-storage/archive"
    project_config["worker_machine"][0]["components_path"] = "/home/bizzflow/worker-context/components"
    project_config["worker_machine"][0]["data_path"] = "/home/bizzflow/worker-context/data"
    project_config["worker_machine"][0]["config_path"] = "/home/bizzflow/worker-context/config"
    project_config["classes"] = {
        "storage_manager": "PostgreSQLStorageManager",
        "sandbox_manager": "PostgreSQLSandboxManager",
        "vault_manager": "AirflowVaultManager",
        "worker_manager": "LocalWorkerManager",
        "file_storage_manager": "LocalFileStorageManager",
        "datamart_manager": "PostgreSQLDatamartManager",
        "credentials_manager": "PostgreSQLCredentialManager",
        "transformation_executor": "PostgreSQLTransformationExecutor",
        "step": "BQStep",
    }
    project_config["storage"] = {"host": "postgres", "database": "bizzflow"}
    transformation_prefix = "postgresql."

# rename transformation folder with proper prefix and deleted the other ones
for transformation_folder in os.listdir(transformations_folder_path):
    if os.path.isdir(os.path.join(transformations_folder_path, transformation_folder)):
        if transformation_folder.startswith(transformation_prefix):
            os.rename(
                os.path.join(transformations_folder_path, transformation_folder),
                os.path.join(transformations_folder_path, transformation_folder[len(transformation_prefix) :]),
            )
        else:
            shutil.rmtree(os.path.join(transformations_folder_path, transformation_folder))


with open(project_config_file_path, "w") as project_config_file:
    json.dump(project_config, project_config_file, indent=4)
